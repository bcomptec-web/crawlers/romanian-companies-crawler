#!/usr/bin/env bash

# ./count_input.sh > input_count.txt
# tail -n +2 input_count.txt | grep -Po '(\d+)(?= .*)' | paste -sd'+' | bc
# 40743

input_counties=("Alba" "Arad" "Arges" "Bacau" "Bihor" "Bistrita-nasaud" "Botosani" "Braila" "Brasov" "Bucuresti" "Buzau" "Calarasi" "Caras-severin" "Cluj" "Constanta" "Covasna" "Dambovita" "Dolj" "Galati" "Giurgiu" "Gorj" "Harghita" "Hunedoara" "Ialomita" "Iasi" "Ilfov" "Maramures" "Mehedinti" "Mures" "Neamt" "Olt" "Prahova" "Salaj" "Satu-mare" "Sibiu" "Suceava" "Teleorman" "Timis" "Tulcea" "Valcea" "Vaslui" "Vrancea") 

echo "Number of counties: ${#input_counties[@]}"

declare -a caen_codes=(6201 6202 7112 5829 6203 6209 6311 6312)

for input_county in "${input_counties[@]}"
do
    echo "$input_county"
    for cod_caen in "${caen_codes[@]}"
    do
        if [[ -s "Data/$input_county/county_companies_${input_county}_${cod_caen}.xls" ]]; then
          wc -l "Data/$input_county/county_companies_${input_county}_${cod_caen}.xls"
        fi
    done
    echo
done
