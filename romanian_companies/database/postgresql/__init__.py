from .postgresqldatabase import PostgreSQLDatabase
import os
import sys
import logging


def create_database():
    try:
        dsn = os.environ['DSN']
        with PostgreSQLDatabase(dsn, debug=True) as db:
            db.create_schema()
    except KeyError as err:
        return


create_database()

__all__ = [PostgreSQLDatabase]
