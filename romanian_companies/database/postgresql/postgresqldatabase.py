import psycopg2
import logging
import json
import os

logger = logging.getLogger(__name__)

class PostgreSQLDatabase:
    def __init__(self, dsn, debug=False):
        self.dsn = dsn
        self.debug = debug
        self.conn = None

    def __enter__(self):
        self.create_connection()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close_connection()

    def _dump_connection_info(self):
        # Print PostgreSQL Connection properties
        logger.info(self.conn.get_dsn_parameters())

        # Print PostgreSQL version
        cursor = self.conn.cursor()
        cursor.execute("SELECT version();")
        logger.info(f"You are connected to - {cursor.fetchone()[0]}")

    def create_connection(self):
        if not self.dsn:
            logger.error("Cannot create database connection. Empty connection string!")
            return
        try:
            self.conn = psycopg2.connect(self.dsn)
            if self.debug:
                logger.info("PostgreSQL connection opened")
                self._dump_connection_info()
        except (Exception, psycopg2.Error) as error:
            logger.error(f"Error while connecting to PostgreSQL: {error}")

    def close_connection(self):
        if self.conn:
            self.conn.close()
            if self.debug:
                logger.info("PostgreSQL connection closed")

    def connection(self):
        if not self.conn:
            self.create_connection()
        return self.conn

    def cursor(self):
        return self.connection().cursor()

    def create_schema(self):
        try:
            with self.conn:
                with self.conn.cursor() as cursor:
                    cursor.execute('''CREATE TABLE IF NOT EXISTS "firme" (
                                        "cui"	BIGINT NOT NULL,
                                        "nume"	TEXT,
                                        "cod_inmatriculare"	TEXT UNIQUE,
                                        "euid"	TEXT UNIQUE,
                                        "stare_onrc" TEXT,
                                        "stare_total_firme" TEXT,
                                        "stare_mfinante" TEXT,
                                        "adresa"	TEXT,
                                        "judet"     TEXT,
                                        "localitate" TEXT,
                                        "cod_caen" INTEGER,
                                        "activitate" TEXT,
                                        "total_firme_url" TEXT,
                                        "mfinante_url" TEXT,
                                        "mfinante_ani_bilant" TEXT,
                                        "mfinante_general_data" JSONB,
                                        "create_time" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                                        "update_time" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                                        PRIMARY KEY("cui"))
                                    ''')
                    cursor.execute('''
                                    CREATE TABLE IF NOT EXISTS "stari_firme_onrc" (
                                        "cod"	INTEGER NOT NULL,
                                        "denumire"	TEXT UNIQUE,
                                        "create_time" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                                        "update_time" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                                        PRIMARY KEY("cod")
                                    );
                    ''')
                    cursor.execute('''
                        CREATE TABLE IF NOT EXISTS "bilant_firme" (
                            "cui" BIGINT NOT NULL REFERENCES "firme"("cui") ON UPDATE CASCADE ON DELETE CASCADE, -- identification number
                            "an" INTEGER NOT NULL, -- year
                            "cifra_de_afaceri" INTEGER, -- turnover
                            "profit_brut" INTEGER, -- gross profit
                            "profit_net" INTEGER, -- net profit
                            "numar_angajati" INTEGER, -- number of employees
                            "mfinante_bilant_data" JSONB,
                            "create_time" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                            "update_time" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                            PRIMARY KEY ("cui", "an")
                        )
                    ''')
        except psycopg2.Error as e:
            logger.error(f"A database error occurred: {e.args[0]}")
            return False
        return True

    def create_caen_schema(self):
        try:
            with self.conn:
                with self.conn.cursor() as cursor:
                    cursor.execute('''
                                    CREATE TABLE IF NOT EXISTS "coduri_caen" (
                                        "cod"	INTEGER NOT NULL,
                                        "denumire"	TEXT UNIQUE,
                                        "create_time" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                                        "update_time" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                                        PRIMARY KEY("cod")
                                    );
                    ''')
        except psycopg2.Error as e:
            logger.error(f"A database error occurred: {e.args[0]}")
            return False
        return True

    def upsert_firma(self, item):
        try:
            with self.conn:
                with self.conn.cursor() as cursor:
                    cursor.execute(
                        "SELECT EXISTS(SELECT 1 FROM firme WHERE cui = %s)", (item.cui,))

                    if not cursor.fetchone()[0]:
                        cursor.execute("""INSERT INTO firme (cui,
                                                            nume,
                                                            cod_inmatriculare,
                                                            euid,
                                                            stare_onrc,
                                                            adresa,
                                                            judet,
                                                            localitate,
                                                            total_firme_url,
                                                            mfinante_url)
                                        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
                                    """,
                                    (item.cui,
                                        item.nume,
                                        item.cod_inmatriculare,
                                        item.euid,
                                        item.stare,
                                        item.adresa,
                                        item.judet,
                                        item.localitate,
                                        item.total_firme_url,
                                        item.mfinante_url))
                    else:
                        # logging.warning(f"Item already exists - {item.cui}, {item.nume}, {item.judet}")
                        cursor.execute("""UPDATE firme
                            SET update_time = CURRENT_TIMESTAMP, nume = %s, cod_inmatriculare = %s, euid = %s, stare_onrc = %s,
                            adresa = %s, judet = %s, localitate = %s, total_firme_url = %s, mfinante_url = %s
                            WHERE cui = %s
                        """, (item.nume, item.cod_inmatriculare, item.euid, item.stare, item.adresa, item.judet, item.localitate,
                            item.total_firme_url, item.mfinante_url, item.cui))
        except psycopg2.Error as e:
            logger.error(f"A database error occurred: {e.args[0]}")
            return False

        return True

    def upsert_stare_firma(self, item):
        try:
            with self.conn:
                with self.conn.cursor() as cursor:
                    cursor.execute(
                        "SELECT EXISTS(SELECT 1 FROM stari_firme_onrc WHERE cod = %s)", (item.cod,))

                    if not cursor.fetchone()[0]:
                        cursor.execute("""INSERT INTO stari_firme_onrc (cod, denumire)
                                        VALUES (%s, %s)
                                    """,
                                    (item.cod, item.denumire))
                    else:
                        # logging.warning(f"Item already exists - {item.cod}, {item.denumire}")
                        cursor.execute("""UPDATE stari_firme_onrc
                            SET update_time = CURRENT_TIMESTAMP, denumire = %s
                            WHERE cod = %s
                        """, (item.denumire, item.cod))
        except psycopg2.Error as e:
            logger.error(f"A database error occurred: {e.args[0]}")
            return False

        return True

    def upsert_caen(self, item):
        if not item:
            return False
        try:
            with self.conn:
                with self.conn.cursor() as cursor:
                    cursor.execute(
                        "SELECT EXISTS(SELECT 1 FROM coduri_caen WHERE cod = %s)", (item.cod,))

                    if not cursor.fetchone()[0]:
                        cursor.execute("""INSERT INTO coduri_caen (cod, denumire)
                                        VALUES (%s, %s)
                                    """,
                                       (item.cod, item.denumire))
                    else:
                        # logging.warning(f"Item already exists - {item.cod}, {item.denumire}")
                        cursor.execute("""UPDATE coduri_caen
                            SET update_time = CURRENT_TIMESTAMP, denumire = %s
                            WHERE cod = %s
                        """, (item.denumire, item.cod))
        except psycopg2.Error as e:
            logger.error(f"A database error occurred: {e.args[0]}")
            return False

        return True

    def update_company_general_data(self, cif, data):
        if not cif or not data:
            return False
        try:
            with self.conn:
                with self.conn.cursor() as cursor:
                    cursor.execute("""UPDATE firme 
                                    SET update_time = CURRENT_TIMESTAMP, 
                                        mfinante_general_data = %s
                                    WHERE cui = %s""", (json.dumps(data, indent=4, sort_keys=True), cif))
        except psycopg2.Error as e:
            logger.error(
                f"A database error occurred while updating company general data: {e.args[0]}")
            return False
        return True

    def upsert_company_balance_sheet(self, cif, balance_sheet_year, balance_sheet_data, revenue, profit_brut, profit_net, number_of_employees):
        if not cif or not balance_sheet_year or not balance_sheet_data:
            return False

        try:
            with self.conn:
                with self.conn.cursor() as cursor:
                    cursor.execute(
                        "SELECT EXISTS(SELECT 1 FROM bilant_firme WHERE cui = %s and an = %s)", (cif, balance_sheet_year))

                    if not cursor.fetchone()[0]:
                        cursor.execute("""INSERT INTO bilant_firme (cui, 
                                                                    an,
                                                                    cifra_de_afaceri, 
                                                                    profit_brut,
                                                                    profit_net, 
                                                                    numar_angajati, 
                                                                    mfinante_bilant_data)
                                        VALUES (%s, %s, %s, %s, %s, %s, %s)
                                    """,
                                        (cif,
                                        balance_sheet_year,
                                        revenue,
                                        profit_brut,
                                        profit_net,
                                        number_of_employees,
                                        json.dumps(balance_sheet_data, indent=4, sort_keys=True)))
                    else:
                        logger.info(
                            f"Updating balance sheet data for company: {cif}, year: {balance_sheet_year}")
                        cursor.execute("""UPDATE bilant_firme
                            SET update_time = CURRENT_TIMESTAMP, cifra_de_afaceri = %s, profit_brut = %s, profit_net = %s, numar_angajati = %s, mfinante_bilant_data = %s
                            WHERE cui = %s AND an = %s
                        """, (revenue,
                            profit_brut,
                            profit_net,
                            number_of_employees,
                            json.dumps(balance_sheet_data, indent=4, sort_keys=True),
                            cif,
                            balance_sheet_year))
        except psycopg2.Error as e:
            logger.error(
                f"A database error occurred while upserting company balance sheet: {e.args[0]}")
            return False

        return True

    def update_company_available_balance_years(self, cif, years):
        if not cif or not years:
            return False
        try:
            with self.conn:
                with self.conn.cursor() as cursor:
                    cursor.execute("""UPDATE firme 
                                    SET update_time = CURRENT_TIMESTAMP, 
                                        mfinante_ani_bilant = %s
                                    WHERE cui = %s""", (','.join([str(year) for year in years]), cif))
        except psycopg2.Error as e:
            logger.error(
                f"A database error occurred while updating company balance years: {e.args[0]}")
            return False
        return True

    def update_company_total_firme_status(self, cif, status):
        if not cif or not status:
            return False
        try:
            with self.conn:
                with self.conn.cursor() as cursor:
                    cursor.execute("""UPDATE firme 
                                    SET update_time = CURRENT_TIMESTAMP, 
                                        stare_total_firme = %s
                                    WHERE cui = %s""", (status, cif))
        except psycopg2.Error as e:
            logger.error(
                f"A database error occurred while updating company status: {e.args[0]}")
            return False
        return True

    def update_company_status(self, cif, status):
        if not cif or not status:
            return False
        try:
            with self.conn:
                with self.conn.cursor() as cursor:
                    cursor.execute("""UPDATE firme 
                                    SET update_time = CURRENT_TIMESTAMP, 
                                        stare_mfinante = %s
                                    WHERE cui = %s""", (status, cif))
        except psycopg2.Error as e:
            logger.error(
                f"A database error occurred while updating company status: {e.args[0]}")
            return False
        return True

    def update_company_activity(self, cif, activity):
        if not cif or not activity:
            return False
        try:
            with self.conn:
                with self.conn.cursor() as cursor:
                    cursor.execute("""UPDATE firme 
                                    SET update_time = CURRENT_TIMESTAMP, 
                                        activitate = %s
                                    WHERE cui = %s""", (activity, cif))
        except psycopg2.Error as e:
            logger.error(
                f"A database error occurred while updating company activity: {e.args[0]}")
            return False
        return True

    def update_company_caen_code(self, cif, caen_code):
        if not cif or not caen_code:
            return False
        try:
            with self.conn:
                with self.conn.cursor() as cursor:
                    cursor.execute("""UPDATE firme 
                                    SET update_time = CURRENT_TIMESTAMP, 
                                        cod_caen = %s
                                    WHERE cui = %s""", (caen_code, cif))
        except psycopg2.Error as e:
            logger.error(
                f"A database error occurred while updating company CAEN code: {e.args[0]}")
            return False
        return True

    def update_caen_code_for_companies_with_activity(self, caen_code, activity):
        if not caen_code or not activity:
            return False
        try:
            with self.conn:
                with self.conn.cursor() as cursor:
                    cursor.execute("""UPDATE firme 
                                    SET update_time = CURRENT_TIMESTAMP, 
                                        cod_caen = %s
                                    WHERE activitate = %s AND cod_caen IS NULL""", (caen_code, activity))
        except psycopg2.Error as e:
            logger.error(
                f"A database error occurred while updating company CAEN code for companies with activity: {e.args[0]}")
            return False
        return True

    def update_activity_for_companies(self, old_activity, new_activity):
        if not old_activity or not new_activity:
            return False
        try:
            with self.conn:
                with self.conn.cursor() as cursor:
                    cursor.execute("""UPDATE firme 
                                    SET update_time = CURRENT_TIMESTAMP, 
                                        activitate = %s
                                    WHERE activitate = %s""", (new_activity, old_activity))
        except psycopg2.Error as e:
            logger.error(
                f"A database error occurred while updating activity for companies: {e.args[0]}")
            return False
        return True

    def get_companies_with_activity_and_no_caen_code(self):
        try:
            with self.conn:
                with self.conn.cursor() as cursor:
                    cursor.execute("""SELECT cui, nume, activitate 
                                      FROM firme 
                                      WHERE activitate IS NOT NULL AND cod_caen IS NULL
                                      ORDER BY cui""")
                    return cursor.fetchall()
        except psycopg2.Error as e:
            logger.error(
                f"A database error occurred while fetching companies with activity and no caen code: {e.args[0]}")
            return False
        return True

    def find_caen_code_by_activity(self, activity):
        if not activity:
            return False

        # 'Fara activitate economica' doesn't have any correspondent
        activity_aliases_map = {
            'Activitati de editare de ghiduri, compendii liste de adrese si similare': 'Activitati de editare de ghiduri, compendii, liste de adrese si similare',
            'Activitati de post-productie cinematografica, video si de programe de televiziune': 'Activitati de postproductie cinematografica, video si de programe de televiziune',
            'Comercializarea combustibililor gazosi, prin conducte': 'Comercializarea combustibililor gazosi prin conducte',
            'Comert cu motociclete, piese si accesorii aferente, intretinerea si repararea motocicletelor': 'Comert cu motociclete, piese si accesorii aferente; intretinerea si repararea motocicletelor',
            'Comert cu ridicata al produselor din ceramica, sticlarie, si produse de intretinere': 'Comert cu ridicata al produselor din ceramica, sticlarie, si al produselor de intretinere',
            'Fabricarea articolelor din fire metalice, fabricarea de lanturi si arcuri': 'Fabricarea articolelor din fire metalice; fabricarea de lanturi si arcuri',
            'Fabricarea produselor metalice obtinute prin deformare plastica, metalurgia pulberilor': 'Fabricarea produselor metalice obtinute prin deformare plastica; metalurgia pulberilor',
            'Invatamant superior non- universitar': 'Invatamant superior nonuniversitar',
            'Productia de profile obtinute la rece': 'Productia de profiluri obtinute la rece',
            'Servicii de furnizare si management a fortei de munca': 'Alte servicii de furnizare a fortei de munca',
            'Tabacirea si finisarea pieilor, prepararea si vopsirea blanurilor': 'Tabacirea si finisarea pieilor; prepararea si vopsirea blanurilor'
        }
        try:
            with self.conn:
                with self.conn.cursor() as cursor:
                    query = """SELECT coalesce( 
                                        (SELECT cod_caen FROM firme WHERE activitate = %s AND cod_caen IS NOT NULL LIMIT 1), 
                                        (SELECT cod as cod_caen FROM coduri_caen WHERE denumire = %s)
                                    ) AS cod_caen 
                                    """
                    cursor.execute(query, (activity, activity))
                    result = cursor.fetchone()
                    if not result or not result[0]:
                        if activity in activity_aliases_map:
                            alias_activity = activity_aliases_map[activity]
                            cursor.execute(query, (alias_activity, alias_activity))
                            result = cursor.fetchone()
                    if result:
                        return result[0]
        except psycopg2.Error as e:
            logger.error(
                f"A database error occurred while finding caen code for activity: {e.args[0]}")
            return False
        return True

    def get_activity_by_caen_code(self, caen_code):
        if not caen_code:
            return False
        try:
            with self.conn:
                with self.conn.cursor() as cursor:
                    cursor.execute("""SELECT coalesce( 
                                        (SELECT activitate FROM firme WHERE cod_caen = %s AND activitate IS NOT NULL LIMIT 1), 
                                        (SELECT denumire as activitate FROM coduri_caen WHERE cod = %s)
                                    ) AS activitate""", (caen_code, caen_code))
                    result = cursor.fetchone()
                    if result:
                        return result[0]
        except psycopg2.Error as e:
            logger.error(
                f"A database error occurred while updating activity for companies: {e.args[0]}")
            return False

    def get_companies_with_broken_ani_bilant(self):
        try:
            with self.conn:
                with self.conn.cursor() as cursor:
                    cursor.execute("""SELECT cui, nume, mfinante_ani_bilant
                                        FROM firme 
                                        WHERE mfinante_ani_bilant ~ '\d,\d,\d,\d'
                                        ORDER BY judet, localitate, nume, cui""")
                    return cursor.fetchall()
        except psycopg2.Error as e:
            logger.error(
                f"A database error occurred while fetching companies with broken ani bilant: {e.args[0]}")
            return False

    def get_companies_by_caen_code(self, caen_code):
        if not caen_code:
            return False
        try:
            with self.conn:
                with self.conn.cursor() as cursor:
                    cursor.execute("""SELECT cui, nume, cod_inmatriculare, judet, localitate, cod_caen, activitate, total_firme_url, mfinante_url
                                      FROM firme 
                                      WHERE cod_caen = %s
                                      ORDER BY judet, localitate, nume, cui""", (caen_code,))
                    return cursor.fetchall()
        except psycopg2.Error as e:
            logger.error(
                f"A database error occurred while fetching companies by caen code: {e.args[0]}")
            return False
    
    def get_companies_by_caen_code_with_balance_in_year(self, caen_code, fiscal_year):
        if not caen_code or not fiscal_year:
            return False
        try:
            with self.conn:
                with self.conn.cursor() as cursor:
                    cursor.execute("""SELECT a.cui, nume, cod_inmatriculare, judet, localitate, cod_caen, activitate, total_firme_url, mfinante_url, b.cifra_de_afaceri, 
                                             CASE WHEN b.profit_brut = 0 THEN (SELECT (mfinante_bilant_data->'cpp'->'pierdere'->>'brut')::int * (-1)) ELSE b.profit_brut END AS profit_brut, 
                                             CASE WHEN b.profit_net = 0 THEN (mfinante_bilant_data->'cpp'->'pierdere'->>'net')::int * (-1) ELSE b.profit_net END AS profit_net, 
                                             b.numar_angajati
                                      FROM firme a JOIN bilant_firme b ON a.cui = b.cui AND b.an = %s
                                      WHERE cod_caen = %s
                                      ORDER BY cifra_de_afaceri DESC, profit_brut DESC, profit_net DESC, numar_angajati DESC, localitate, nume""", (fiscal_year, caen_code))
                    return cursor.fetchall()
        except psycopg2.Error as e:
            logger.error(
                f"A database error occurred while fetching companies by caen code with balance in year: {e.args[0]}")
            return False