from romanian_companies.database.sqlite import SQLiteDatabase
from romanian_companies.database.postgresql import PostgreSQLDatabase


class Database(PostgreSQLDatabase):
    def __init__(self, dsn):
        super().__init__(dsn=dsn)


__all__ = [Database, SQLiteDatabase, PostgreSQLDatabase]
