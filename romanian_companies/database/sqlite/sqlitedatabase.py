import sqlite3
import logging
import json

logger = logging.getLogger(__name__)


class SQLiteDatabase:
    def __init__(self, db=None):
        self.db = db if db is not None else 'Data/RomanianCompanies.db'
        self.conn = None

    def __enter__(self):
        self.create_connection()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close_connection()

    def create_connection(self):
        # https://www.sqlite.org/uri.html
        # https://docs.python.org/3.8/library/sqlite3.html#sqlite3.Connection
        # isolation level = None for autocommit mode or one of “DEFERRED”, “IMMEDIATE” or “EXCLUSIVE”
        self.conn = sqlite3.connect('file:{0}?mode=rwc'.format(self.db), isolation_level=None, uri=True)
        self.conn.execute('PRAGMA journal_mode = WAL;')

    def close_connection(self):
        if self.conn:
            self.conn.close()
            logger.info("SQLite connection closed")

    def connection(self):
        if not self.conn:
            self.create_connection()
        return self.conn

    def cursor(self):
        return self.connection().cursor()

    def create_schema(self):        
            try:
                with self.conn:
                    self.conn.execute('''CREATE TABLE IF NOT EXISTS "firme" (
                                        "cui"	BIGINT NOT NULL,
                                        "nume"	TEXT,
                                        "cod_inmatriculare"	TEXT UNIQUE,
                                        "euid"	TEXT UNIQUE,
                                        "stare_onrc" TEXT,
                                        "stare_total_firme" TEXT,
                                        "stare_mfinante" TEXT,
                                        "adresa"	TEXT,
                                        "judet"     TEXT,
                                        "localitate" TEXT,
                                        "cod_caen" INTEGER,
                                        "activitate" TEXT,
                                        "total_firme_url" TEXT,
                                        "mfinante_url" TEXT,
                                        "mfinante_ani_bilant" TEXT,
                                        "mfinante_general_data" JSON,
                                        "create_time" DATETIME DEFAULT CURRENT_TIMESTAMP,
                                        "update_time" DATETIME DEFAULT CURRENT_TIMESTAMP,
                                        PRIMARY KEY("cui"))                              
                                    ''')
                    self.conn.execute('''
                                    CREATE TABLE IF NOT EXISTS "stari_firme_onrc" (
                                        "cod"	INTEGER NOT NULL,
                                        "denumire"	TEXT UNIQUE,
                                        "create_time" DATETIME DEFAULT CURRENT_TIMESTAMP,
                                        "update_time" DATETIME DEFAULT CURRENT_TIMESTAMP,
                                        PRIMARY KEY("cod")
                                    );  
                    ''')
                    self.conn.execute('''
                        CREATE TABLE IF NOT EXISTS "bilant_firme" (
                            "cui" BIGINT NOT NULL REFERENCES "firme"("cui") ON UPDATE CASCADE ON DELETE CASCADE, -- identification number
                            "an" INTEGER NOT NULL, -- year
                            "cifra_de_afaceri" INTEGER, -- turnover
                            "profit_brut" INTEGER, -- gross profit
                            "profit_net" INTEGER, -- net profit
                            "numar_angajati" INTEGER, -- number of employees
                            "mfinante_bilant_data" JSON,
                            "create_time" DATETIME DEFAULT CURRENT_TIMESTAMP,
                            "update_time" DATETIME DEFAULT CURRENT_TIMESTAMP,
                            PRIMARY KEY ("cui", "an")
                        )
                    ''')
            except sqlite3.Error as e:
                logger.error(f"An SQLite error occurred: {e.args[0]}")
                return False
            return True


    def get_companies(self):
        try:
            return self.conn.execute('SELECT * FROM firme')
        except sqlite3.Error as e:
            logger.error(f"An SQLite error occurred: {e.args[0]}")
            return False

    def get_balances(self):
        try:
            return self.conn.execute('SELECT * FROM bilant_firme')
        except sqlite3.Error as e:
            logger.error(f"An SQLite error occurred: {e.args[0]}")
            return False

    def upsert_firma(self, item):
        try:
            with self.conn:
                cursor = self.conn.execute(
                    "SELECT EXISTS(SELECT 1 FROM firme WHERE cui = ?)", (item.cui,))

                if not cursor.fetchone()[0]:
                    self.conn.execute("""INSERT INTO firme (cui,
                                                        nume,
                                                        cod_inmatriculare,
                                                        euid,
                                                        stare_onrc,
                                                        adresa,
                                                        judet,
                                                        localitate,
                                                        total_firme_url,
                                                        mfinante_url)
                                    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                                """,
                                (item.cui,
                                    item.nume,
                                    item.cod_inmatriculare,
                                    item.euid,
                                    item.stare,
                                    item.adresa,
                                    item.judet,
                                    item.localitate,
                                    item.total_firme_url,
                                    item.mfinante_url))
                else:
                    # logging.warning(f"Item already exists - {item.cui}, {item.nume}, {item.judet}")
                    self.conn.execute("""UPDATE firme
                        SET update_time = CURRENT_TIMESTAMP, nume = ?, cod_inmatriculare = ?, euid = ?, stare_onrc = ?,
                        adresa = ?, judet = ?, localitate = ?, total_firme_url = ?, mfinante_url = ?
                        WHERE cui = ?
                    """, (item.nume, item.cod_inmatriculare, item.euid, item.stare, item.adresa, item.judet, item.localitate,
                        item.total_firme_url, item.mfinante_url, item.cui))
        except sqlite3.Error as e:
            logger.error(f"An SQLite error occurred: {e.args[0]}")
            return False

        return True

    def upsert_stare_firma(self, item):
        try:
            with self.conn:
                cursor = self.conn.execute(
                    "SELECT EXISTS(SELECT 1 FROM stari_firme_onrc WHERE cod = ?)", (item.cod,))

                if not cursor.fetchone()[0]:
                    cursor.execute("""INSERT INTO stari_firme_onrc (cod, denumire)
                                    VALUES (?, ?)
                                """,
                                (item.cod, item.denumire))
                else:
                    # logging.warning(f"Item already exists - {item.cod}, {item.denumire}")
                    self.conn.execute("""UPDATE stari_firme_onrc
                        SET update_time = CURRENT_TIMESTAMP, denumire = ?
                        WHERE cod = ?
                    """, (item.denumire, item.cod,))
        except sqlite3.Error as e:
            logger.error(f"An SQLite error occurred: {e.args[0]}")
            return False

        return True

    def update_company_general_data(self, cif, data):
        if not cif or not data:
            return
        try:
            with self.conn:
                self.conn.execute("""UPDATE firme 
                                SET update_time = CURRENT_TIMESTAMP, 
                                    mfinante_general_data = ?
                                WHERE cui = ?""", (json.dumps(data, indent=4, sort_keys=True), cif))
        except sqlite3.Error as e:
            logger.error(
                f"An SQLite error occurred while updating company general data: {e.args[0]}")
            return False
        return True

    def upsert_company_balance_sheet(self, cif, balance_sheet_year, balance_sheet_data, revenue, profit_brut, profit_net, number_of_employees):
        if not cif or not balance_sheet_year or not balance_sheet_data:
            return
        
            try:
                with self.conn:
                    cursor = self.conn.execute(
                        "SELECT EXISTS(SELECT 1 FROM bilant_firme WHERE cui = ? and an = ?)", (cif, balance_sheet_year))

                    if not cursor.fetchone()[0]:
                        logger.info(
                            f"Inserting balance sheet data for company: {cif}, year: {balance_sheet_year}")
                        self.conn.execute("""INSERT INTO bilant_firme (cui, 
                                                                    an,
                                                                    cifra_de_afaceri, 
                                                                    profit_brut,
                                                                    profit_net, 
                                                                    numar_angajati, 
                                                                    mfinante_bilant_data)
                                        VALUES (?, ?, ?, ?, ?, ?, ?)
                                    """,
                                    (cif,
                                        balance_sheet_year,
                                        revenue,
                                        profit_brut,
                                        profit_net,
                                        number_of_employees,
                                        json.dumps(balance_sheet_data, indent=4, sort_keys=True)))
                    else:
                        logger.info(f"Updating balance sheet data for company: {cif}, year: {balance_sheet_year}")
                        self.conn.execute("""UPDATE bilant_firme
                            SET update_time = CURRENT_TIMESTAMP, cifra_de_afaceri = ?, profit_brut = ?, profit_net = ?, numar_angajati = ?, mfinante_bilant_data = ?
                            WHERE cui = ? AND an = ?
                        """, (revenue,
                            profit_brut,
                            profit_net,
                            number_of_employees,
                            json.dumps(balance_sheet_data, indent=4, sort_keys=True),
                            cif, 
                            balance_sheet_year))
            except sqlite3.Error as e:
                logger.error(
                    f"An SQLite error occurred while upserting company balance sheet: {e.args[0]}")
                return False

        return True

    def update_company_available_balance_years(self, cif, years):
        if not cif or not years:
            return
        try:
            with self.conn:
                self.conn.execute("""UPDATE firme 
                                SET update_time = CURRENT_TIMESTAMP, 
                                    mfinante_ani_bilant = ?
                                WHERE cui = ?""", (','.join([str(year) for year in years]), cif))
        except sqlite3.Error as e:
            logger.error(
                f"An SQLite error occurred while updating company balance years: {e.args[0]}")
            return False
        return True

    def update_company_status(self, cif, status):
        if not cif or not status:
            return        
        try:
            with self.conn:
                self.conn.execute("""UPDATE firme 
                                SET update_time = CURRENT_TIMESTAMP, 
                                    stare_mfinante = ?
                                WHERE cui = ?""", (status, cif))
        except sqlite3.Error as e:
            logger.error(
                f"An SQLite error occurred while updating company status: {e.args[0]}")
            return False
        return True

    def update_company_activity(self, cif, activity):
        if not cif or not activity:
            return
        try:
            with self.conn:
                self.conn.execute("""UPDATE firme 
                                SET update_time = CURRENT_TIMESTAMP, 
                                    activitate = ?
                                WHERE cui = ?""", (activity, cif))
        except sqlite3.Error as e:
            logger.error(
                f"An SQLite error occurred while updating company activity: {e.args[0]}")
            return False
        return True

    def update_company_caen_code(self, cif, caen_code):
        if not cif or not caen_code:
            return        
        try:
            with self.conn:
                self.conn.execute("""UPDATE firme 
                                SET update_time = CURRENT_TIMESTAMP, 
                                    cod_caen = ?
                                WHERE cui = ?""", (caen_code, cif))
        except sqlite3.Error as e:
            logger.error(
                f"An SQLite error occurred while updating company CAEN code: {e.args[0]}")
            return False
        return True
