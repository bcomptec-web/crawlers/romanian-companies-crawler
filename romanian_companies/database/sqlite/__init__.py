from .sqlitedatabase import SQLiteDatabase


def create_database():
    with SQLiteDatabase() as db:
        db.create_schema()


create_database()

__all__ = [SQLiteDatabase]
