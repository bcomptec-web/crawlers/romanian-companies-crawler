import logging
import os
from romanian_companies import CrawlerUtils
from romanian_companies.database import SQLiteDatabase, PostgreSQLDatabase

logger = logging.getLogger(__name__)


class DataSync(object):
    def __init__(self, dsn):
        self.dsn = dsn

    def sync(self):
        with SQLiteDatabase() as sqliteDB:
            with PostgreSQLDatabase(self.dsn) as postgresDB:
                companies = sqliteDB.get_companies()
                for company in companies:
                    self._sync_company(company, postgresDB)

                balances = sqliteDB.get_balances()
                for balance in balances:
                    self._sync_balance(balance, postgresDB)

    def _sync_company(self, company, db):
        cui = company[0]
        stare_mfinante = company[6]
        activitate = company[11]
        mfinante_ani_bilant = company[14]
        mfinante_general_data = company[15]
        db.update_company_general_data(cui, mfinante_general_data)
        db.update_company_available_balance_years(cui, mfinante_ani_bilant)
        db.update_company_status(cui, stare_mfinante)
        db.update_company_activity(cui, activitate)

    def _sync_balance(self, balance, db):
        cui = balance[0]
        an = balance[1]
        cifra_de_afaceri = balance[2]
        profit_brut = balance[3]
        profit_net = balance[4]
        numar_angajati = balance[5]
        mfinante_bilant_data = balance[6]
        db.upsert_company_balance_sheet(
            cui, an, mfinante_bilant_data, cifra_de_afaceri, profit_brut, profit_net, numar_angajati)

if __name__ == '__main__':
    CrawlerUtils.configure_logger()
    ds = DataSync(os.environ['DSN'])
    ds.sync()
