import logging
import sqlite3
import csv
from romanian_companies import CrawlerUtils
from romanian_companies.database import Database

logger = logging.getLogger(__name__)


class ONRCImporter(object):
    class RowStareFirma(object):
        def __init__(self, cod, denumire):
            self.cod = cod
            self.denumire = denumire

        def __getitem__(self, key):
            return getattr(self, key)

    class RowFirma(object):
        def __init__(self, cui, nume, cod_inmatriculare, euid, stare, adresa, judet, localitate, total_firme_url, mfinante_url):
            self.cui = cui
            self.nume = nume
            self.cod_inmatriculare = cod_inmatriculare
            self.euid = euid
            self.stare = stare
            self.adresa = adresa
            self.judet = judet
            self.localitate = localitate
            self.total_firme_url = total_firme_url
            self.mfinante_url = mfinante_url

        def __getitem__(self, key):
            return getattr(self, key)

    def __init__(self, counties, dsn, import_company_statuses, import_companies):
        self.dsn = dsn
        self.counties = counties
        self.import_company_statuses = import_company_statuses
        self.import_companies = import_companies

    def _import_company_statuses(self, db):
        with open("Data/data.gov.ro/5nomenclator_stari_firma.csv", newline='', encoding='utf-8') as f:
            csv_reader = csv.reader(f, delimiter='|')
            next(csv_reader, None)  # skip the headers
            try:
                for row in csv_reader:
                    cod = int(row[0].strip())
                    denumire = row[1].strip()
                    row_obj = ONRCImporter.RowStareFirma(cod, denumire)
                    if not db.upsert_stare_firma(row_obj):
                        CrawlerUtils.exit(1)
            except Exception as ex:
                logger.error(ex)

    def _import_companies(self, db):
        for county in self.counties:
            input_file = f'Data/companies_per_county/{county}.csv'

            with open(input_file, newline='', encoding='utf-8') as f:
                csv_reader = csv.reader(f, delimiter='^')
                # next(csv_reader, None)  # skip the headers

                try:
                    for row in csv_reader:
                        nume = row[0].strip()
                        cui = row[1].strip()
                        cod_inmatriculare = row[2].strip()
                        euid = row[3].strip()
                        stare = row[4].strip()
                        adresa = row[5].strip()
                        judet = county
                        localitate = CrawlerUtils.strip_diacritics(
                            CrawlerUtils.parse_locality_from_address(adresa))
                        total_firme_url = CrawlerUtils.get_total_firme_url(
                            nume, cui)
                        mfinante_url = CrawlerUtils.get_mfinante_url(cui)
                        raw_obj = ONRCImporter.RowFirma(cui, nume, cod_inmatriculare, euid, stare, adresa, judet,
                                                        localitate, total_firme_url, mfinante_url)
                        if not db.upsert_firma(raw_obj):
                            CrawlerUtils.exit(1)
                except Exception as ex:
                    logger.error(ex)

    def run(self):
        with Database(self.dsn) as db:
            if self.import_company_statuses:
                self._import_company_statuses(db)
            if self.import_companies:
                self._import_companies(db)
