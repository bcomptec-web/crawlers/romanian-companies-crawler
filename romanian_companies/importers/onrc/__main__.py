import logging
import argparse
import os
from romanian_companies.importers.onrc import ONRCImporter
from romanian_companies import CrawlerUtils

CrawlerUtils.configure_logger()
logger = logging.getLogger(__name__)


def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Importer for ONRC data from data.gov.ro')
    parser.add_argument('counties', type=str, nargs='+',
                        help='the list of counties to import data for (mandatory)')
    parser.add_argument('--import_company_statuses', type=str2bool, nargs='?', const=True,
                        default=True, help='controls whether to import company statuses (default: True)')
    parser.add_argument('--import_companies',
                        type=str2bool, nargs='?', const=True, default=True, help='controls whether to import companies (default: True)')
    args = parser.parse_args()

    counties = args.counties
    dsn = os.environ['DSN']

    if not dsn or not counties:
        parser.print_help()
        CrawlerUtils.exit(1)

    logger.info("Counties: {}, Import Company Statuses: {}, Import Companies: {}".format(str(counties), args.import_company_statuses, args.import_companies))
    
    onrc_importer = ONRCImporter(
        counties, dsn, args.import_company_statuses, args.import_companies)
    onrc_importer.run()
