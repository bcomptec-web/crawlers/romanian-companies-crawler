import logging
import argparse
import os
from romanian_companies.importers.caen import CAENImporter
from romanian_companies import CrawlerUtils

CrawlerUtils.configure_logger()
logger = logging.getLogger(__name__)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Importer for CAEN data from data.gov.ro')
    parser.add_argument('input_file', type=str, help='the file with CAEN data (mandatory)')
    args = parser.parse_args()

    try:
        dsn = os.environ['DSN']
    except KeyError as err:
        logger.error("Missing DSN environment variable")
        CrawlerUtils.exit(1)

    if not dsn or not args.input_file:
        parser.print_help()
        CrawlerUtils.exit(1)

    logger.info("Input file: {}".format(args.input_file))

    caen_importer = CAENImporter(dsn, args.input_file)
    caen_importer.run()
