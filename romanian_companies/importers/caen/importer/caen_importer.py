import logging
import sqlite3
import csv
from romanian_companies import CrawlerUtils
from romanian_companies.database import Database

logger = logging.getLogger(__name__)


class CAENImporter(object):
    class RowCAEN(object):
        def __init__(self, cod, denumire):
            self.cod = cod
            self.denumire = denumire

        def __getitem__(self, key):
            return getattr(self, key)

    def __init__(self, dsn, input_file):
        self.dsn = dsn
        self.input_file = input_file

    def _import_caen(self, db):
        db.create_caen_schema()

        with open(self.input_file, newline='', encoding='utf-8') as f:
            csv_reader = csv.reader(f, delimiter=';', quoting=csv.QUOTE_MINIMAL)
            next(csv_reader, None)  # skip the headers
            try:
                for row in csv_reader:
                    cod = int(row[0].strip().replace(',00', ''))
                    denumire = CrawlerUtils.remove_multiple_spaces(CrawlerUtils.strip_diacritics(row[8].strip()))
                    row_obj = CAENImporter.RowCAEN(cod, denumire)
                    if not db.upsert_caen(row_obj):
                        CrawlerUtils.exit(1)
            except Exception as ex:
                logger.error(ex)

    def run(self):
        with Database(self.dsn) as db:
           self._import_caen(db)
