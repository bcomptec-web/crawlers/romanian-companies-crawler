import logging
import argparse
import os
from romanian_companies.importers.caen import CAENProcessor
from romanian_companies import CrawlerUtils

CrawlerUtils.configure_logger()
logger = logging.getLogger(__name__)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Importer for CAEN data from data.gov.ro')
    args = parser.parse_args()

    try:
        dsn = os.environ['DSN']
    except KeyError as err:
        logger.error("Missing DSN environment variable")
        CrawlerUtils.exit(1)

    caen_processor = CAENProcessor(dsn)
    caen_processor.run()
