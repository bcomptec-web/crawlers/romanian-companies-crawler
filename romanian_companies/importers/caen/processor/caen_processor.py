import logging
import sqlite3
import csv
from romanian_companies import CrawlerUtils
from romanian_companies.database import Database

logger = logging.getLogger(__name__)


class CAENProcessor(object):
    def __init__(self, dsn):
        self.dsn = dsn

    def unify_activities(self, db):
        """
        select cui, cod_caen, mfinante_ani_bilant from firme where activitate = '';

        See Data/queries.sql for SQL queries on how to find similar activities programatically
        """

        # Total Firme ~> MFinante
        activities_map = {
            'Activitati ale agentilor si brokerilor de asigurari': 'Activitati ale agentilor si broker-ilor de asigurari',

            'Activitati auxiliare intermedierilor financiare, exclusiv activitati de asigurari si fonduri de pensii': 'Activitati auxiliare intermedierilor financiare, exclusiv activitati de asigurari sifonduri de pensii',

            'Activitati de contabilitate si audit financiar; consultanta in domeniul fiscal': 'Activitati de contabilitate si audit financiar, consultanta in domeniul fiscal',

            'Activitati specializate de curatenie a cladirilor, mijloacelor de transport, masinilor si utilajelor industriale': 'Activitati specializate de curatenie a cladirilor, mijloacelor de transport, masini si utilaje industriale',

            'Alte activitati auxiliare activitatilor de asigurari si fonduri de pensii': 'Alte activitati auxiliare de asigurari si fonduri de pensii',

            'Cercetare-dezvoltare in alte stiinte naturale si inginerie': 'Cercetare- dezvoltare in alte stiinte naturale si inginerie',

            'Cercetare-dezvoltare in biotehnologie': 'Cercetare- dezvoltare in biotehnologie',

            'Cercetare-dezvoltare in stiinte sociale si umaniste': 'Cercetare- dezvoltare in stiinte sociale si umaniste',

            'Activitati de management (gestiune si exploatare) al mijloacelor de calcul': 'Activitati de management (gestiune si exploatare) a mijloacelor de calcul',

            'Comert cu amanuntul al calculatoarelor, unitatilor periferice si software-ului, in magazine specializate': 'Comert cu amanuntul al calculatoarelor, unitatilor periferice si software-ului in magazine specializate',

            'Comert cu amanuntul al carburantilor pentru autovehicule, in magazine specializate': 'Comert cu amanuntul al carburantilor pentru autovehicule in magazine specializate',

            'Comert cu amanuntul al echipamentului audio/ video, in magazine specializate': 'Comert cu amanuntul al echipamentului audio/ video in magazine specializate',

            'Comert cu amanuntul al echipamentului pentru telecomunicatii, in magazine specializate': 'Comert cu amanuntul al echipamentului pentru telecomunicatii in magazine specializate',

            'Comert cu amanuntul prin intermediul caselor de comenzi sau prin internet': 'Comert cu amanuntul prin intermediul caselor de comenzi sau prin Internet',

            'Comert cu ridicata al produselor lactate, oualor, uleiurilor si grasimilor comestibile': 'Comert cu ridicata al produselor lactate, oualelor, uleiurilor si grasimilor comestibile',

            'Extractia pietrisului si nisipului; extractia argilei si caolinului': 'Extractia pietrisului si nisipului, extractia argilei si caolinului',

            'Fabricarea altor produse din lemn; fabricarea articolelor din pluta, paie si din alte materiale vegetale impletite': 'Fabricarea altor produse din lemn, fabricarea articolelor din pluta, paie si din alte materiale vegetale impletite',

            'Fabricarea anvelopelor si a camerelor de aer; resaparea si refacerea anvelopelor': 'Fabricarea anvelopelor si a camerelor de aer, resaparea si refacerea anvelopelor',

            'Fabricarea biscuitilor si piscoturilor; fabricarea prajiturilor si a produselor conservate de patiserie': 'Fabricarea biscuitilor si piscoturilor, fabricarea prajiturilor si a produselor conservate de patiserie',

            'Fabricarea de suruburi, buloane si alte articole filetate; fabricarea de nituri si saibe': 'Fabricarea de suruburi, buloane si alte articole filetate, fabricarea de nituri si saibe',

            'Fabricarea painii; fabricarea prajiturilor si a produselor proaspete de patiserie': 'Fabricarea painii, fabricarea prajiturilor si a produselor proaspete de patiserie',

            'Inchirierea si subinchirierea bunurilor imobiliare proprii sau in leasing': 'Inchirierea si subinchirierea bunurilor imobiliare proprii sau inchiriate',

            'Intermedieri in comertul cu materii prime agricole, animale vii, materii prime textile si cu bunuri semifinite': 'Intermedieri in comertul cu materii prime agricole, animale vii, materii prime textile si cu semifabricate',

            'Productia de bauturi racoritoare nealcoolice; productia de ape minerale si alte ape imbuteliate': 'Productia de bauturi racoritoare nealcoolice, productia de ape minerale si alte ape imbuteliate',

            'Productia de caroserii pentru autovehicule; fabricarea de remorci si semiremorci': 'Productia de caroserii pentru autovehicule, fabricarea de remorci si semiremorci',

            'Repararea aparatelor electronice, de uz casnic': 'Repararea aparatelor electrocasnice, de uz casnic',

            'Alte servicii de furnizare a fortei de munca': 'Servicii de furnizare si management a fortei de munca',

            'Comert cu ridicata al produselor din ceramica, sticlarie, si al produselor de intretinere': 'Comert cu ridicata al produselor din ceramica, sticlarie, si produse de intretinere'
        }
        for old, new in activities_map.items():
            db.update_activity_for_companies(old, new)

    def match_caen_code_for_activities(self, db):
        companies = db.get_companies_with_activity_and_no_caen_code()
        logger.info(f"Number of companies to process: {len(companies)}")

        for company in companies:
            cui, nume, activity = company[0], company[1], company[2]

            caen_code = db.find_caen_code_by_activity(activity)
            # logger.info(f"CAEN is {caen_code} for: {cui} - {nume} with activity {activity}")

            db.update_caen_code_for_companies_with_activity(caen_code, activity)

    def fix_mfinante_ani_bilant(self, db):
        companies = db.get_companies_with_broken_ani_bilant()
        logger.info(f"Number of companies with broken ani bilant: {len(companies)}")

        for company in companies:
            cui, nume, mfinante_ani_bilant = company
            years_str = CrawlerUtils.normalize_mfinante_ani_bilant(mfinante_ani_bilant)
            # logger.info(f"[{cui} - {nume}] Updating mfinante ani bilanti from {mfinante_ani_bilant} to {years_str}")
            db.update_company_available_balance_years(cui, years_str.split(','))

    def run(self):
        with Database(self.dsn) as db:
            self.unify_activities(db)
            self.match_caen_code_for_activities(db)
            # self.fix_mfinante_ani_bilant(db)