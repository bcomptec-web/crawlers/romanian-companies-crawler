import unittest
from romanian_companies.crawler_utils import CrawlerUtils


class CrawlerUtilsTests(unittest.TestCase):
    def test_get_total_firme_url(self):
        test_cases = [
            ('ANDRION D & G SRL', '36488345', 'andrion-dg-srl-36488345'),
            ('C & P DELUXE-INVEST SRL', '37151070', 'cp-deluxe-invest-srl-37151070'),
            ('CRY & RO SRL', '16067988', 'cry-ro-srl-16067988'),
            ('TATA&FIUL CONSTRUCȚII ȘI RENOVĂRI S.R.L.', '42229779', 'tata-fiul-constructii-si-renovari-srl-42229779'),
            ('AM&HC DELIVERY S.R.L.', '41364157', 'am-hc-delivery-srl-41364157'),
            ('I.D. INVESTMENT SRL', '20848766', 'id-investment-srl-20848766'),
            ('F.D. & R.C. INSTALGAZ SRL', '25143448', 'fdrc-instalgaz-srl-25143448'),
            ('D`OTTO MEDIA MARKETING S.R.L.', '42426533', 'dotto-media-marketing-srl-42426533'),
            ('A&C GOOAL SRL', '38352764', 'ac-gooal-srl-38352764')
        ]
        for index, test_case in enumerate(test_cases):
            want = 'https://www.totalfirme.ro/' + test_case[2]
            got = CrawlerUtils.get_total_firme_url(test_case[0], test_case[1])
            with self.subTest(i=index, n=test_case[0]):
                self.assertEqual(got, want)

    def test_strip_diacritics(self):
        test_cases = [("Braşov", "Brasov"), ("Sânpetru", "Sanpetru"),
                      ("Zărneşti", "Zarnesti"), ("Făgăraş", "Fagaras"), ("Tărlungeni", "Tarlungeni")]
        for index, test_case in enumerate(test_cases):
            want = test_case[1]
            got = CrawlerUtils.strip_diacritics(test_case[0])
            with self.subTest(i=index, n=test_case[0]):
                self.assertEqual(got, want)

    def test_normalize_mfinante_ani_bilat(self):
        test_cases = [("2,0,1,4", "2014"), 
                      ("2,0,1,4,,,2,0,1,5", "2014,2015"),
                      ("2,0,1,4,,,2,0,1,5,,,2,0,1,6", "2014,2015,2016"),
                      ("2,0,1,4,,,2,0,1,5,,,2,0,1,6,,,2,0,1,7,,,2,0,1,8,,,2,0,1,9", "2014,2015,2016,2017,2018,2019"),
                      ("2019", "2019"),
                      ("2014,2015,2016,2017,2018,2019", "2014,2015,2016,2017,2018,2019")]
        for index, test_case in enumerate(test_cases):
            want = test_case[1]
            got = CrawlerUtils.normalize_mfinante_ani_bilant(test_case[0])
            with self.subTest(i=index, n=test_case[0]):
                self.assertEqual(got, want)

if __name__ == '__main__':
    unittest.main()
