import unicodedata
import datetime
import json
import logging
import os
from sys import platform, exit
import traceback
import time
import semidbm
import subprocess
from selenium import webdriver
import re
from random_user_agent.user_agent import UserAgent
from random_user_agent.params import SoftwareName, OperatingSystem, HardwareType, SoftwareType, Popularity

logger = logging.getLogger(__name__)

_RE_COMBINE_WHITESPACE = re.compile(r"\s+")
_RE_MFINANTE_ANI_BILANT = re.compile(r"(\d,\d,\d,\d)")

class CrawlerUtils(object):
    @staticmethod
    def exit(exit_code=0):
        exit(exit_code)

    @staticmethod
    def exception_to_string(excp):
        stack = traceback.extract_stack()[:-3] + traceback.extract_tb(excp.__traceback__)  # add limit=??
        pretty = traceback.format_list(stack)
        return ''.join(pretty) + '\n  {} {}'.format(excp.__class__, excp)

    @staticmethod
    def clear_shelve(name):
        if not os.path.exists(name):
            return
        db = semidbm.open(name, 'c')
        keys = [key for key in db]
        for key in keys:
            del db[key]
        db.close()

    @staticmethod
    def store_shelve_data(name, key, value):
        # Note: value should be -2 from actual, i.e. if you want it to start from row 1363, it should be 1361
        value -= 2
        db = semidbm.open(name, 'c')
        db[key] = str(value)
        db.close()

    @staticmethod
    def dump_shelve(name):
        db = semidbm.open(name, 'r')
        for k in db:
            print(k, db[k])
        db.close()

    @staticmethod
    def is_unix():
        return platform in ["linux", "linux2", "darwin"]

    @staticmethod
    def is_windows():
        return platform in ["win32", "win64"]

    @staticmethod
    def create_virtual_display():
        from pyvirtualdisplay import Display
        display = Display(visible=0, size=(1920, 1080))
        display.start()
        return display

    @staticmethod
    def create_google_chrome_driver(name, verbose=False):
        chrome_driver_path = CrawlerUtils.get_chrome_driver_path()
        logger.info(f"Chrome driver path: {chrome_driver_path}")

        chrome_binary_path = CrawlerUtils.get_chrome_binary_path()
        logger.info(f"Chrome binary path: {chrome_binary_path}")

        chrome_options = CrawlerUtils.get_chrome_options(chrome_binary_path)

        service_args = [f'--log-path=chromedriver_${name}.log']
        if verbose:
            service_args.append('--verbose')

        return webdriver.Chrome(executable_path=chrome_driver_path, options=chrome_options, service_args=service_args)

    @staticmethod
    def get_chrome_options(chrome_binary_path):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.binary_location = chrome_binary_path

        # TODO: Recreate driver after TimeoutException with a different user_agent
        user_agent_rotator = CrawlerUtils.get_user_agent_rotator()
        user_agent = user_agent_rotator.get_random_user_agent()
        logger.info(f"Using User-Agent: {user_agent}")
        chrome_options.add_argument(f'--user-agent={user_agent}')

        # chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-gpu')
        # chrome_options.add_argument('--enable-logging')
        # chrome_options.add_argument('--dump-dom')
        # chrome_options.add_argument('--proxy-server={0}'.format("10.0.x.x:yyyy"))

        return chrome_options

    @staticmethod
    def get_chrome_binary_path():
        if CrawlerUtils.is_windows():
            binary_location = r'C:\Program Files\Google\Chrome\Application\chrome.exe'
            if not os.path.exists(binary_location):
                return r'C:\Program Files (x86)\Google\Chrome\Application\chrome.exe'
        else:
            # /usr/bin/chromium
            # /usr/bin/google-chrome-stable
            # /usr/bin/google-chrome-beta
            # /usr/bin/google-chrome-unstable
            if os.path.exists('/usr/bin/chromium'):
                return '/usr/bin/chromium'        
            elif os.path.exists('/usr/bin/google-chrome-stable'):
                return '/usr/bin/google-chrome-stable'
            elif os.path.exists('/usr/bin/google-chrome'):
                return '/usr/bin/google-chrome'
            elif os.path.exists('/snap/bin/chromium'):
                return '/snap/bin/chromium'
            else:
                raise FileNotFoundError()

    @staticmethod
    def get_chrome_driver_path():
        current_dir = os.path.dirname(os.path.realpath(__file__))
        is_windows = CrawlerUtils.is_windows()

        if is_windows:
            return current_dir + os.sep + 'chromedriver.exe'

        # Linux, system driver
        if os.path.exists("/usr/bin/chromedriver") and os.path.exists('/usr/bin/chromium'):
            return "/usr/bin/chromedriver"
        else:  # local driver downloaded from https://chromedriver.chromium.org/downloads
            return current_dir + os.sep + ('chromedriver.exe' if is_windows else 'chromedriver')

    @staticmethod
    def get_user_agent_rotator():
        """
        https://github.com/Luqman-Ud-Din/random_user_agent
        https://developers.whatismybrowser.com/useragents/explore/
        https://github.com/hellysmile/fake-useragent
        :return: string user agent
        """
        # you can also import SoftwareEngine, HardwareType, SoftwareType, Popularity from random_user_agent.params
        # you can also set number of user agents required by providing `limit` as parameter

        software_names = [SoftwareName.CHROME.value,
                          SoftwareName.FIREFOX.value,
                          SoftwareName.SAFARI.value,
                          SoftwareName.EDGE.value,
                          SoftwareName.INTERNET_EXPLORER.value,
                          SoftwareName.OPERA.value,
                          ]

        operating_systems = [OperatingSystem.WINDOWS.value,
                             OperatingSystem.LINUX.value,
                             OperatingSystem.MACOS.value,
                             OperatingSystem.ANDROID.value,
                             OperatingSystem.CHROMEOS.value,
                             OperatingSystem.FREEBSD.value,
                             OperatingSystem.IOS.value,
                             OperatingSystem.OPENBSD.value,
                             ]
        hardware_types = [
            HardwareType.COMPUTER.value,
            HardwareType.MOBILE.value,
        ]

        software_types = [
            SoftwareType.WEB_BROWSER.value,
        ]

        popularity = [
            Popularity.POPULAR.value,
            Popularity.COMMON.value,
            Popularity.AVERAGE.value,
        ]

        return UserAgent(software_names=software_names,
                         operating_systems=operating_systems,
                         hardware_types=hardware_types,
                         software_types=software_types,
                         popularity=popularity,
                         limit=20000)

    @staticmethod
    def get_last_valid_fiscal_year():
        last_valid_fiscal_year = int((datetime.datetime.now() - datetime.timedelta(days=365)).strftime("%Y"))
        # print("Last valid fiscal year: {}".format(last_valid_fiscal_year))
        return last_valid_fiscal_year

    @staticmethod
    def simple_line_count(filename):
        lines = 0
        for line in open(filename, encoding="utf-8"):
            lines += 1
        return lines

    @staticmethod
    def wc(filename):
        if CrawlerUtils.is_windows():
            return CrawlerUtils.simple_line_count(filename)
        else:
            out = subprocess.Popen(['wc', '-l', filename],
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.STDOUT
                                   ).communicate()[0]
            return int(out.partition(b' ')[0])

    @staticmethod
    def dump_json(data):
        logger.info(json.dumps(data, indent=4, sort_keys=True))

    @staticmethod
    def dump_input(caen_codes, counties):
        logger.info("CAEN codes:")
        CrawlerUtils.dump_json(caen_codes)

        logger.info("Counties:")
        CrawlerUtils.dump_json(counties)

        exit(0)

    @staticmethod
    def retry_func(func, max_retry=10, sleep_factor=5):
        """
        @param func: The function that needs to be retried
        @param max_retry: How many times `func` function should be retried before giving up
        @param sleep_factor: By how many seconds to incrementally backoff at each retry
        @return: func the function to retry
        @raise: RetryException if the number of retries exceeds max_retry parameter value
        """
        exception = None
        for retry in range(1, max_retry + 1):
            try:
                if retry > 1:
                    sleep_time = sleep_factor * retry
                    logger.info("Retry #{} Sleeping {}".format(retry, sleep_time))
                    time.sleep(sleep_time)
                return func()
            except Exception as e:
                exception = e
                logger.warning('Failed to call {}, in retry({}/{}) - {}'.format(func.func, retry, max_retry, str(e)))
        else:
            raise RetryException(exception, max_retry)

    @staticmethod
    def get_total_firme_url(name, cif):
        name = CrawlerUtils.strip_diacritics(name)
        
        url = f"{name}-{cif}".lower()
        url = url.replace('. & ', '')
        url = url.replace('.', '')
        url = url.replace("'", '')

        and_regex = re.compile(r'(\w+) & (\w+)')
        match = re.search(and_regex, url)
        if match:            
            if len(match.group(1)) == 1 and len(match.group(2)) == 1:
                url = url.replace(' & ', '')
            else:
                url = url.replace(' & ', '-')

        url = url.replace('& ', '')
        url = url.replace(' - ', '')
        url = url.replace(' ', '-')
        url = url.replace('--', '-')

        and_regex = re.compile(r'(\w+)&(\w+)')
        match = re.search(and_regex, url)
        if match:            
            if len(match.group(1)) > 1 and len(match.group(2)) >= 1:
                url = url.replace('&', '-')

        invalid_chars_regex = re.compile(r'[&\"()`]')
        url = re.sub(invalid_chars_regex, '', url)

        return f'https://www.totalfirme.ro/{url}'

    @staticmethod
    def parse_locality_from_address(address):
        parts = address.split(',')

        locality = ''
        if parts and len(parts):
            locality = parts[0]
            prefix = 'Municipiul '
            if locality.startswith(prefix):
                locality = locality[len(prefix):]
                return locality

            prefix = 'Sat '
            if locality.startswith(prefix):
                locality = locality[len(prefix):]
                return locality

            prefix = 'Comuna '
            if locality.startswith(prefix):
                locality = locality[len(prefix):]
                return locality

            prefix = 'Loc. '
            if locality.startswith(prefix):
                locality = locality[len(prefix):]
                return locality

            prefix = 'Oraş '
            if locality.startswith(prefix):
                locality = locality[len(prefix):]
                return locality

        return locality.strip()

    @staticmethod
    def get_mfinante_url(cui):
        return f"https://www.mfinante.gov.ro/infocodfiscal.html?cod={cui}"

    @staticmethod
    def strip_diacritics(text):
        """
        Strip diacritics/accents from input string.

        :param text: The input string.
        :type text: String.

        :returns: The processed String.
        :rtype: String.
        """
        text = unicodedata.normalize('NFD', text)
        text = text.encode('ascii', 'ignore')
        text = text.decode("utf-8")
        return str(text)

    @staticmethod
    def remove_multiple_spaces(text):
        return _RE_COMBINE_WHITESPACE.sub(" ", text).strip()

    @staticmethod
    def configure_logger(logfile=None):
        if logfile and logfile == "/dev/stdout":
            logfile = ""

        LOGGER_FORMAT = '%(asctime)s %(filename)s: ' \
            '%(levelname)s: ' \
            '%(funcName)s(): ' \
            '%(lineno)d:\t' \
            '%(message)s'

        logging.basicConfig(level=logging.INFO, format=LOGGER_FORMAT, filename=logfile)

    @staticmethod
    def normalize_mfinante_ani_bilant(ani_bilant_str):
        if not ani_bilant_str:
            return

        matches = re.findall(_RE_MFINANTE_ANI_BILANT, ani_bilant_str)

        output = []
        for match in matches:
            processed_match = match.replace(',', '').strip()
            output.append(processed_match)

        if len(output) == 0:
            output.append(ani_bilant_str)

        return ','.join(output)

class RetryException(Exception):
    u_str = "Exception ({}) raised after {} tries."

    def __init__(self, exception, max_retry):
        self.exception = exception
        self.max_retry = max_retry

    def __unicode__(self):
        return self.u_str.format(self.exception, self.max_retry)

    def __str__(self):
        return self.__unicode__()
