#!/usr/bin/env python3

import argparse
import logging
import os
from romanian_companies.crawlers.total_firme import TotalFirmeScraper
from romanian_companies import CrawlerUtils

CrawlerUtils.configure_logger()
logger = logging.getLogger(__name__)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Crawler for totalfirme.ro')
    parser.add_argument('--sleep_time', metavar='sleep_time', type=int, default=10,
                        help='the sleep time in seconds between requests (optional)')
    parser.add_argument('--fiscal_year', metavar='fiscal_year', type=int,
                        default=CrawlerUtils.get_last_valid_fiscal_year(),
                        help='the fiscal year to fetch bilant for (optional)')
    parser.add_argument('--counties', metavar='counties', type=str, nargs='+',
                        help='the list of counties to get data for (mandatory)')
    parser.add_argument(
        '--input_file',
        type=str,
        help='Path to default input file (optional)',
        nargs='?'
    )
    args = parser.parse_args()

    sleep_time = args.sleep_time
    fiscal_year = args.fiscal_year
    counties = args.counties
    input_file = args.input_file
    dsn = os.environ['DSN']

    if not counties:
        parser.print_help()
        CrawlerUtils.exit(1)

    logger.info("Sleep time: {0}s, Fiscal year: {1}, Counties: {2}".format(sleep_time,
                                                                           fiscal_year,
                                                                           str(counties)))
                                                                           
    with TotalFirmeScraper(input_file, dsn, counties, fiscal_year, sleep_time) as scraper:
        scraper.run()
