import csv
import logging
import sys
import time
import tempfile
import semidbm
import re
import os
from pathlib import Path
from selenium.common.exceptions import NoSuchElementException
from romanian_companies.crawler_utils import CrawlerUtils
from romanian_companies.database import Database

logger = logging.getLogger(__name__)
shelf_file = Path(tempfile.gettempdir()) / "shelf_total_firme"

#CrawlerUtils.clear_shelve(shelf_file)
#CrawlerUtils.store_shelve_data(shelf_file, f'Data/companies_per_county/Brasov/firme3.csv', 151)


class TotalFirmeScraper(object):
    NAME = 'total_firme_scraper'
    CAEN_REGEX = re.compile(r'^(?P<activitate>.*) \(CAEN: (?P<cod_caen>\d+)\)$')

    def __init__(self, input_file, dsn, counties, fiscal_year, nap_time):
        self.input_file = input_file
        self.dsn = dsn
        self.counties = counties
        self.fiscal_year = fiscal_year
        self.nap_time = nap_time
        self.not_found_fh = None

    def __enter__(self):
        self.driver = CrawlerUtils.create_google_chrome_driver(TotalFirmeScraper.NAME)
        self._open_not_found_file()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.driver.quit()
        self._close_not_found_file()

    def _open_not_found_file(self):
        self.not_found_fh = open("total_firme_not_found.csv", "a")

    def _close_not_found_file(self):
        if self.not_found_fh:
            self.not_found_fh.close()

    def _write_not_found(self, name, cif, url):
        if not self.not_found_fh:
            return
        self.not_found_fh.write( (",".join([name, cif, url])) + "\n")

    def parse_page(self, name, cif, url):
        number_of_employees = revenue = profit_brut = 0
        activitate = ''
        cod_caen = ''
        stare = ''
        try:
            logger.info("Getting data for company: {} - {} from {}".format(name, cif, url))
            self.driver.get(url)
            
            logger.debug(self.driver.page_source)

            try:
                not_found_element = self.driver.find_element_by_xpath('/html/body/div/div[1]/div/p')
                not_found_element = not_found_element.text.strip()
                if not_found_element == 'Sorry, the page you are looking for could not be found.':                    
                    logger.info(f"Not found: {name}, {cif}, {url}")
                    self._write_not_found(name, cif, url)
                    return
            except NoSuchElementException:
                pass

            # Stare
            stare = self.driver.find_element_by_xpath('/html/body/article/div[1]/div[1]/p[1]').text
            if stare:
                stare = stare.replace('Stare firmă: ', '')

            # Activitate and CAEN
            caen = self.driver.find_element_by_xpath('/html/body/article/div[1]/div[1]/p[3]').text
            if caen:
                caen = caen.replace('Obiect de activitate: ', '')
                activitate, cod_caen = TotalFirmeScraper.parse_caen(caen)

            logger.info(f"Name: {name} has Activitate: {activitate}, CAEN: {cod_caen}, Stare: {stare}")

            last_fiscal_year = self.driver.find_element_by_xpath('/html/body/article/div[1]/div[2]/h4/span')
            last_fiscal_year = int(last_fiscal_year.text)
            logger.info("Last fiscal year for company: {0}".format(last_fiscal_year))

            if last_fiscal_year == self.fiscal_year:
                logger.info("Company has balance sheet for last valid fiscal year: {}".format(last_fiscal_year))
            else:
                logger.info("Company has balance sheet most recently in fiscal year: {}".format(last_fiscal_year))

            # Numar de angajati
            number_of_employees = self.driver.find_element_by_xpath('/html/body/article/div[1]/div[2]/p[1]')
            number_of_employees = number_of_employees.text.strip()
            if number_of_employees:
                parts = number_of_employees.split(" ")
                if len(parts) >= 4:
                    number_of_employees = parts[3]

            # Cifra de afaceri
            revenue = self.driver.find_element_by_xpath('/html/body/article/div[1]/div[2]/p[2]')
            revenue = revenue.text.strip()
            if revenue:
                parts = revenue.split(" ")
                if len(parts) >= 5:
                    revenue = parts[4].replace('.', '')

            # Profit
            profit_brut = self.driver.find_element_by_xpath('/html/body/article/div[1]/div[2]/p[3]')
            profit_brut = profit_brut.text.strip()
            if profit_brut:
                parts = profit_brut.split(" ")
                if len(parts) >= 3:
                    profit_brut = parts[2].replace('.', '')

            return [number_of_employees, revenue, profit_brut, activitate, cod_caen, stare]
        except NoSuchElementException as ex:
            logger.warning("Company: {} - {} doesn't have balance sheet in last valid year fiscal year: {} - {}".format(
                name, cif, self.fiscal_year, url))
            logger.debug(CrawlerUtils.exception_to_string(ex))
        except Exception as ex:
            logger.fatal("Error: {}".format(sys.exc_info()))
        
        return [number_of_employees, revenue, profit_brut, activitate, cod_caen, stare]

    @staticmethod
    def parse_caen(caen):
        activitate = ''
        cod_caen = ''
        match = re.match(TotalFirmeScraper.CAEN_REGEX, caen)
        if match:
            activitate = match.group(1)
            cod_caen = match.group(2)
        return activitate, cod_caen

    class Result:
        def __init__(self, name, cif, mat, revenue, profit, number_of_employees, activity, caen, status, url):
            self.name = name
            self.cif = cif
            self.mat = mat
            self.revenue = revenue
            self.profit = profit
            self.number_of_employees = number_of_employees
            self.activity = activity
            self.caen = caen
            self.status = status
            self.url = url

    def _run(self, county, input_file):
        if not Path(input_file).is_file():
            logger.warning(f"No data for: {input_file}")
            return

        number_of_companies = CrawlerUtils.wc(input_file)
        logger.info(
            f"Number of companies to process: {number_of_companies} in file: {input_file}")

        with open(input_file) as input_csv_file:
            csv_reader = csv.reader(input_csv_file, delimiter='^')

            with Database(self.dsn) as db:
                start_row = None
                semidb = semidbm.open(shelf_file, 'c')
                if input_file.encode() in semidb:
                    start_row = int(semidb[input_file])
                    logger.info(
                        "Start row should be: {}".format(start_row + 2))
                    del semidb[input_file]
                semidb.close()

                line_count = 0

                for row in csv_reader:
                    # Skip rows until the row we stopped previously
                    if start_row and line_count <= start_row:
                        line_count += 1
                        continue
                    else:
                        line_count += 1

                    start = time.time()

                    name = row[0]
                    cif = row[1]
                    mat = row[2]

                    logger.info(
                        f"[{line_count}/{number_of_companies}] Processing {name} - {cif}")
                    url = CrawlerUtils.get_total_firme_url(name, cif)

                    try:
                        result = self.parse_page(name, cif, url)

                        if result:
                            number_of_employees = result[0]
                            revenue = result[1]
                            profit = result[2]
                            activitate = CrawlerUtils.strip_diacritics(result[3])
                            caen = result[4]
                            stare = result[5]

                            if stare == 'Ascuns':
                                raise KeyboardInterrupt

                            logger.info(
                                f"[{name} - {cif}] Number of employees: {number_of_employees}, Revenue: {revenue}, Profit brut: {profit}, Activitate: {activitate}, CAEN: {caen}, Stare: {stare}")

                            self._process_result(TotalFirmeScraper.Result(name, cif, mat, revenue, profit, number_of_employees, activitate, caen, stare, url), db)

                        end = time.time()
                        elapsed = end - start
                        logger.info("{} Elapsed time: {} (s.ms)".format(
                            line_count, elapsed))
                        logger.info("Napping: {}s".format(self.nap_time))
                        time.sleep(self.nap_time)
                    except KeyboardInterrupt:
                        # NOTE: CTRL + C should only be called when napping
                        semidb = semidbm.open(shelf_file, 'c')
                        semidb[input_file] = str(line_count - 2)
                        semidb.close()
                        logger.info(
                            f"Saved last row before exit: {line_count}")
                        exit(2)
                logger.info(f'Processed {line_count} lines.')

    def _process_result(self, result, db):
        # Stare
        db.update_company_total_firme_status(result.cif, result.status)

        # Activitate
        db.update_company_activity(result.cif, result.activity)

        # CAEN code
        db.update_company_caen_code(result.cif, result.caen)

    def run(self):
        for county in self.counties:
            # input_file = f'Data/companies_per_county_{self.fiscal_year}/{county}.csv'
            input_file = f'Data/companies_per_county/{county}.csv'
            self._run(county, self.input_file if self.input_file else input_file)
            if self.input_file:
                break
