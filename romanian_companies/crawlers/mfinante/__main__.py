#!/usr/bin/env python3

import argparse
import logging
import os
from romanian_companies.crawlers.mfinante import MFinanteScraper
from romanian_companies import CrawlerUtils


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Crawler for mfinante.ro')

    parser.add_argument(
        '--input_file',
        type=str,
        help='Path to default input file (optional)',
        nargs='?'
    )

    parser.add_argument('--sleep_time', metavar='sleep_time', type=int, default=2,
                        help='the sleep time in seconds between requests (optional)')
    parser.add_argument('--fiscal_year', metavar='fiscal_year', type=int,
                        default=CrawlerUtils.get_last_valid_fiscal_year(),
                        help='the fiscal year to fetch bilant for (optional)')
    parser.add_argument('--counties', metavar='counties', type=str, nargs='+',
                        help='the list of counties to get data for (mandatory)')
    parser.add_argument('--caen_codes', metavar='caen_codes', type=int, nargs='*',
                        help='the list of CAEN codes to get data for (optional)')
    parser.add_argument('--logfile', metavar='logfile', type=str, default="/dev/stdout",
                        help='the file where the logs output will be written, defaults to stdout (optional)')
    args = parser.parse_args()

    sleep_time = args.sleep_time
    fiscal_year = args.fiscal_year
    counties = args.counties
    caen_codes = args.caen_codes
    logfile = args.logfile
    input_file = args.input_file

    CrawlerUtils.configure_logger(logfile)

    if not counties:
        parser.print_help()
        CrawlerUtils.exit(1)

    dsn = os.environ['DSN']

    logger = logging.getLogger(__name__)
    logger.info("Sleep time: {0}s, Fiscal year: {1}, CAEN codes: {2}, Counties: {3}, Input file: {4}".format(sleep_time,
                                                                                            fiscal_year,
                                                                                            str(caen_codes),
                                                                                            str(counties),
                                                                                            input_file))

    # test_run(last_valid_fiscal_year, 21878854)
    
    scraper = MFinanteScraper(
        input_file, dsn, sleep_time, counties, fiscal_year, caen_codes)

    try:
        scraper.run()
    except Exception as ex:
        logger.exception("Exception on main handler")
        CrawlerUtils.exit(4)

    CrawlerUtils.exit(0)
