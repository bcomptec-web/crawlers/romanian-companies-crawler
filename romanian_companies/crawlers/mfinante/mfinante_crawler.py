import csv
import logging
import tempfile
import time
import traceback
from collections import defaultdict
from functools import partial
from pathlib import Path

import semidbm
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.wait import WebDriverWait

from romanian_companies.crawler_utils import CrawlerUtils
from romanian_companies.database import Database

logger = logging.getLogger(__name__)
shelf_file = Path(tempfile.gettempdir()) / "shelf_mfinante"

# Use the following code, if you want to run again starting at a certain row in the input file
#CrawlerUtils.clear_shelve(shelf_file)
# CrawlerUtils.store_shelve_data(shelf_file, f'Data/{county}/county_companies_{county}_{caen_code}.xls', 2)
#CrawlerUtils.store_shelve_data(shelf_file, f'Data/companies_per_county/Brasov/xaa.csv', 1132)
# CrawlerUtils.store_shelve_data(shelf_file, r'.\Data\companies_per_county\Brasov\xac.csv', 115)
# CrawlerUtils.dump_shelve(shelf_file)
# CrawlerUtils.exit(1)

class MFinanteCompanyGeneralDataScraper:
    URL_PATH = "https://www.mfinante.gov.ro/infocodfiscal.html?cod={0}"
    TIMEOUT = 15
    ELEMENT_TAG = 'center'

    def __init__(self, driver):
        self.driver = driver

    def scrape_pretty_data(self, url):
        return self.scrape_link(url=url)

    def scrape_raw_data(self, url):
        return self.scrape_link(url,
                                raw=True)

    # Parse raw cells and related rows and append to data general data
    @staticmethod
    def parse_raw_general_data(first_cell, second_cell, data, brother, great_brother):
        logger.debug("First cell: {}, Second cell: {}".format(first_cell, second_cell))

        if second_cell:
            first_cell = first_cell.lower()

            prefix_key = None
            key = None
            value = CrawlerUtils.remove_multiple_spaces(second_cell.strip())

            if 'denumire' in first_cell:  # Denumire platitor:
                key = 'denumire'
            elif 'adresa' in first_cell:  # Adresa:
                key = 'adresa'
            elif 'judet' in first_cell:  # Judetul:
                key = 'judet'
            elif 'inmatriculare' in first_cell:  # Numar de inmatriculare la Registrul Comertului:
                key = 'numar_inmatriculare_onrc'
                value = value.replace(' ', '')
            elif 'autorizare' in first_cell:  # Act autorizare:
                key = 'act_autorizare'
            elif 'postal' in first_cell:  # Codul postal:
                key = 'cod_postal'
            elif 'telefon' in first_cell:  # Telefon:
                key = 'telefon'
            elif 'fax' in first_cell:  # Fax:
                key = 'fax'
            elif 'stare' in first_cell:  # Stare societate:
                prefix_key = 'status'
                key = 'stare'
            elif 'observatii' in first_cell:  # Observatii privind societatea comerciala:
                key = 'observatii'
            elif 'ultimei declaratii' in first_cell:  # Data inregistrarii ultimei declaratii: (*)
                prefix_key = 'status'
                key = 'data_ultimei_declaratii'            
            elif 'ultimei prelucrari' in first_cell:  # Data ultimei prelucrari: (**)
                prefix_key = 'status'
                key = 'data_ultimei_prelucrari'            
            elif 'impozit pe profit' in first_cell:  # Impozit pe profit (data luarii in evidenta):
                prefix_key = 'impozite'
                key = 'impozit_pe_profit'
            # Impozit pe veniturile microintreprinderilor (data luarii in evidenta):
            elif 'impozit pe veniturile microintreprinderilor' in first_cell:
                prefix_key = 'impozite'
                key = 'impozit_pe_venituri_microintreprinderi'
            elif 'accize' in first_cell:  # Accize (data luarii in evidenta):
                prefix_key = 'taxe'
                key = 'accize'
            elif 'taxa pe valoarea adaugata' in first_cell:  # Taxa pe valoarea adaugata (data luarii in evidenta):
                prefix_key = 'taxe'
                key = 'taxa_pe_valoare_adaugata'
            # Contributiile de asigurari sociale (data luarii in evidenta):
            elif 'contributiile de asigurari sociale' in first_cell:
                prefix_key = 'contributii'
                key = 'contributie_asigurari_sociale'
            elif 'munca' in first_cell:  # Contributia asiguratorie pentru munca (data luarii in evidenta):
                prefix_key = 'contributii'
                key = 'contributie_asiguratorie_munca'
            elif 'asigurari sociale de sanatate' in first_cell:  # Contributia de asigurari sociale de sanatate(data luarii in evidenta):
                prefix_key = 'contributii'
                key = 'contributie_asigurari_sanatate'
            # Taxa jocuri de noroc (data luarii in evidenta):
            elif 'noroc' in first_cell:
                prefix_key = 'taxe'
                key = 'taxa_pe_jocuri_noroc'
            # Impozit pe veniturile din salarii si asimilate salariilor (data luarii in evidenta):
            elif 'impozit pe veniturile din salarii' in first_cell:
                prefix_key = 'impozite'
                key = 'impozit_pe_venituri_salarii'
            # Impozit pe constructii(data luarii in evidenta):
            elif 'constructii' in first_cell:
                prefix_key = 'impozite'
                key = 'impozit_pe_constructii'
            # Impozit la titeiul si la gazele naturale din productia interna (data luarii in evidenta):
            elif 'titei' in first_cell:
                prefix_key = 'impozite'
                key = 'impozit_titei_gaze'
            # Redevente miniere/Venituri din concesiuni si inchirieri (data luarii in evidenta):
            elif 'inchirieri' in first_cell:
                prefix_key = 'redevente'
                key = 'redevente_miniere_venituri_inchirieri'
            # Redevente petroliere (data luarii in evidenta):
            elif 'petroliere' in first_cell:
                prefix_key = 'redevente'
                key = 'redevente_petroliere'

            if key:
                if prefix_key:
                    data[prefix_key][key] = value
                else:
                    data[key] = value

        return data

    def scrape_link(self, url, raw=False):
        self.driver.get(url)

        # recursive defaultdict(dict)
        def recursive_dict():
            return defaultdict(recursive_dict)

        data = defaultdict(recursive_dict)
        error = None

        try:
            # wait until element found on page
            WebDriverWait(self.driver, MFinanteCompanyGeneralDataScraper.TIMEOUT).until(
                lambda driver: driver.find_element_by_tag_name(MFinanteCompanyGeneralDataScraper.ELEMENT_TAG))

            element = self.driver.find_element_by_tag_name(MFinanteCompanyGeneralDataScraper.ELEMENT_TAG)

            # start parsing the table
            brother = None
            great_brother = None
            for row in element.find_elements_by_tag_name('tr'):
                if len(row.find_elements_by_tag_name('td')) == 2:
                    first_cell = row.find_elements_by_tag_name('td')[0].text
                    second_cell = row.find_elements_by_tag_name('td')[1].text

                    if raw:
                        data[first_cell] = second_cell
                    else:
                        data = self.parse_raw_general_data(
                            first_cell, second_cell, data, brother, great_brother)

                    # remember related cells
                    if brother:
                        great_brother = brother
                    brother = row

            data['bilant'] = {}
            option_elements = self.driver.find_elements_by_xpath('/html/body/div/div[3]/div[3]/form/select/option')
            if option_elements:
                for option_element in option_elements:
                    key = option_element.get_attribute('value')
                    value = option_element.text
                    data['bilant'][int(value)] = key
        except TimeoutException as ex:
            error = ex.__class__.__name__
            logger.warning(traceback.format_exc())
        except Exception as ex:
            error = ex.__class__.__name__
            logger.warning(traceback.format_exc())
        finally:
            # self.driver.close()
            pass

        return {
            'data': data,
            'error': error
        }


class MFinanteCompanyBalanceSheetDataScraper:
    URL_PATH = "https://www.mfinante.gov.ro/infocodfiscal.html?an={0}&cod={1}&method.bilant={2}&captcha=null"
    DOCUMENT_METHOD = "VIZUALIZARE"
    ELEMENT_TAG = "center"
    TIMEOUT = 15

    def __init__(self, driver):
        self.driver = driver

    # Parse raw cells and related rows and append to data accounting data
    @staticmethod
    def parse_raw_accounting_data(first_cell, second_cell, data, brother, great_brother):
        if second_cell:
            first_cell = first_cell.lower()

            # Indicatori din BILANT
            # active
            if 'imobilizate' in first_cell:  # ACTIVE IMOBILIZATE - TOTAL
                data['bilant']['active']['categories']['imobilizate'] = int(second_cell)
            elif 'circulante' in first_cell:  # ACTIVE CIRCULANTE - TOTAL, din care
                data['bilant']['active']['categories']['circulante']['total'] = int(second_cell)
            elif 'stocuri' in first_cell:  # Stocuri (materii prime, materiale, productie in curs de executie, semifabricate, produse finite, marfuri etc.)
                data['bilant']['active']['categories']['circulante']['categories']['stocuri'] = int(second_cell)
            elif 'creante' in first_cell:  # Creante
                data['bilant']['active']['categories']['circulante']['categories']['creante'] = int(second_cell)
            elif 'casa' in first_cell:  # Casa si conturi la banci
                data['bilant']['active']['categories']['circulante']['categories']['casa'] = int(second_cell)

            # avans
            elif 'cheltuieli in avans' in first_cell:  # CHELTUIELI IN AVANS
                data['bilant']['cheltuieli_in_avans'] = int(second_cell)
            elif 'venituri in avans' in first_cell:  # VENITURI IN AVANS
                data['bilant']['venituri_in_avans'] = int(second_cell)

            # datorii
            elif 'datorii' in first_cell:
                data['bilant']['datorii'] = int(second_cell)
            elif 'provizioane' in first_cell:  # PROVIZIOANE
                data['bilant']['provizioane'] = int(second_cell)

            # capitaluri
            elif 'capitaluri' in first_cell:  # CAPITALURI - TOTAL, din care:
                data['bilant']['capitaluri']['total'] = int(second_cell)
            elif 'subscris' in first_cell:  # Capital subscris varsat
                data['bilant']['capitaluri']['categories']['subscris'] = int(second_cell)
            elif 'patrimoniul regiei' in first_cell:  # Patrimoniul regiei
                data['bilant']['capitaluri']['categories']['patrimoniul_regiei'] = int(second_cell)
            elif 'patrimoniul public' in first_cell:  # Patrimoniul public
                data['bilant']['capitaluri']['categories']['patrimoniul_public'] = int(second_cell)

            # Indicatori din CONTUL DE PROFIT SI PIERDERE
            elif 'cifra de afaceri' in first_cell:  # Cifra de afaceri neta
                data['cpp']['cifra_de_afaceri_neta'] = int(second_cell)
            elif 'venituri' in first_cell:  # VENITURI TOTALE
                data['cpp']['venituri_totale'] = int(second_cell)
            elif 'cheltuieli' in first_cell:  # CHELTUIELI TOTALE
                data['cpp']['cheltuieli_totale'] = int(second_cell)

            # profituri
            elif 'profit' in first_cell:
                if 'brut' in brother.find_elements_by_tag_name('td')[0].text:  # Profit brut
                    data['cpp']['profit']['brut'] = int(second_cell)
                elif 'net' in brother.find_elements_by_tag_name('td')[0].text:  # Profit net
                    data['cpp']['profit']['net'] = int(second_cell)

            # pierderi
            elif 'pierdere' in first_cell:
                if 'brut' in great_brother.find_elements_by_tag_name('td')[0].text:  # Pierdere bruta
                    data['cpp']['pierdere']['brut'] = int(second_cell)
                elif 'net' in great_brother.find_elements_by_tag_name('td')[0].text:  # Pierdere neta
                    data['cpp']['pierdere']['net'] = int(second_cell)

            # Indicatori din DATE INFORMATIVE
            elif 'salariati' in first_cell:  # Numar mediu de salariati
                data['informatii']['salariati'] = int(second_cell)
            elif 'caen' in first_cell:  # Tipul de activitate, conform clasificarii CAEN
                data['informatii']['caen'] = second_cell

        return data

    def scrape_pretty_data(self, url):
        return self.scrape_link(url)

    def scrape_raw_data(self, url):
        return self.scrape_link(url, raw=True)

    def scrape_link(self, url, raw=False):
        self.driver.get(url)

        # recursive defaultdict(dict)
        def recursive_dict():
            return defaultdict(recursive_dict)

        data = defaultdict(recursive_dict)
        data['declara_ca_nu_are_activitate'] = False
        error = None

        try:
            # wait until element found on page
            WebDriverWait(self.driver, MFinanteCompanyBalanceSheetDataScraper.TIMEOUT).until(
                lambda driver: driver.find_element_by_tag_name(MFinanteCompanyBalanceSheetDataScraper.ELEMENT_TAG))

            element = self.driver.find_element_by_tag_name(MFinanteCompanyBalanceSheetDataScraper.ELEMENT_TAG)

            # start parsing the table
            brother = None
            great_brother = None
            for row in element.find_elements_by_tag_name('tr'):
                if len(row.find_elements_by_tag_name('td')) == 2:
                    first_cell = row.find_elements_by_tag_name('td')[0].text
                    second_cell = row.find_elements_by_tag_name('td')[1].text

                    # clean - from second cell
                    if second_cell == '-':
                        second_cell = 0

                    if raw:
                        data[first_cell] = second_cell
                    else:
                        data = self.parse_raw_accounting_data(
                            first_cell, second_cell, data, brother, great_brother)

                    # remember related cells
                    if brother:
                        great_brother = brother
                    brother = row
        except TimeoutException as ex:
            error = ex.__class__.__name__
            logger.warning(traceback.format_exc())
            try:
                if self.driver.find_element_by_xpath(
                        "//*[contains(text(), 'Declaratie pe proprie raspundere ca entitatea nu a desfasurat activitate de la data infiintarii si pana la')]"):
                    data['declara_ca_nu_are_activitate'] = True
            except Exception:
                logger.warning("Cannot find declaratie pe propria raspundere")
        except Exception as ex:
            error = ex.__class__.__name__
            logger.warning(traceback.format_exc())
        finally:
            # self.driver.close()
            pass

        return {
            'data': data,
            'error': error
        }


class MFinanteScraper(object):
    NAME = 'mfinante_scraper'

    def __init__(self, input_file, dsn, nap_time, counties, fiscal_year, caen_codes):
        self.input_file = input_file
        self.dsn = dsn
        self.counties = counties
        self.fiscal_year = fiscal_year
        self.nap_time = nap_time
        self.caen_codes = caen_codes
        self.number_of_companies = 0
        self.number_of_companies_with_revenue = 0
        self.driver = None

    @staticmethod
    def scrape_company_general_data(driver, cui, raw=False, retry=False):
        my_url = MFinanteCompanyGeneralDataScraper.URL_PATH.format(cui)

        general_scraper = MFinanteCompanyGeneralDataScraper(driver)

        if raw:
            scraper_result = general_scraper.scrape_raw_data(my_url)
        else:
            scraper_result = general_scraper.scrape_pretty_data(my_url)

        response = {
            'request': {
                'cui': cui,
                'url': my_url,
            },
            'response': {
                'success': False if scraper_result['error'] else True,
                'result': scraper_result['data'],
                'error': scraper_result['error']
            }
        }

        if retry and response['response']['error'] == 'TimeoutException':
            raise TimeoutException

        return response

    @staticmethod
    def scrape_company_balance_sheet_data(driver, year, cui, raw=False):
        my_url = MFinanteCompanyBalanceSheetDataScraper.URL_PATH.format(year, cui,
                                                                        MFinanteCompanyBalanceSheetDataScraper.DOCUMENT_METHOD)
        balance_sheet_scraper = MFinanteCompanyBalanceSheetDataScraper(driver)

        if raw:
            scraper_result = balance_sheet_scraper.scrape_raw_data(my_url)
        else:
            scraper_result = balance_sheet_scraper.scrape_pretty_data(my_url)

        response = {
            'request': {
                'cui': cui,
                'year': year,
                'url': my_url,
            },
            'response': {
                'success': False if scraper_result['error'] else True,
                'result': scraper_result['data'],
                'error': scraper_result['error']
            }
        }
        return response

    def _run_caen_codes(self):
        for caen_code in self.caen_codes:
            for county in self.counties:
                self._run(county, self.input_file if self.input_file else f'Data/{county}/county_companies_{county}_{caen_code}.csv')

    def _run_counties(self):
        for county in self.counties:
            # input_file = f'Data/companies_per_county_{self.fiscal_year}/{county}.csv'
            self._run(county, self.input_file if self.input_file else f'Data/companies_per_county/{county}.csv')
            if self.input_file:
                break

    def run(self):
        if self.caen_codes:
            self._run_caen_codes()
        else:
            self._run_counties()

    class Company:
        def __init__(self, number, name, cif, mat, address, county, locality, mfinante_url, total_firme_url):
            self.number = number
            self.name = name
            self.cif = cif
            self.mat = mat
            self.address = address
            self.county = county
            self.locality = locality
            self.mfinante_url = mfinante_url
            self.total_firme_url = total_firme_url

    def _fetch_general_company_data(self, company):
        general_data_response = CrawlerUtils.retry_func(
            partial(self.scrape_company_general_data, self.driver, company.cif,
                    raw=False, retry=True),
            max_retry=3, sleep_factor=10)
        # CrawlerUtils.dump_json(general_data_response)

        return general_data_response

    def _fetch_balance_sheet_data(self, company, year):
        logger.info(
            f"Getting balance sheet data for {company.name} ~ {company.cif} in year: {year}")

        balance_sheet_response = self.scrape_company_balance_sheet_data(self.driver, year, company.cif)

        # CrawlerUtils.dump_json(balance_sheet_response)

        return balance_sheet_response

    def _get_balance_sheet_year(self, company, bilant):
        if not bilant:
            return

        # Check if company has balance sheet in desired year
        if self.fiscal_year in bilant:
            latest_balance_year = self.fiscal_year
            balance_sheet_year = bilant[latest_balance_year]
            logger.info(
                f"Company: {company.name} has balance in desired year: {self.fiscal_year} - {balance_sheet_year}")
        else:  # Use most recent year when there is no balance sheet in desired year
            latest_balance_year = max(bilant)
            balance_sheet_year = bilant[latest_balance_year]
            logger.info(
                f"Company: {company.name} has balance most recently in year: {latest_balance_year} - {balance_sheet_year}")
        return (balance_sheet_year, latest_balance_year)

    def _find_caen_code_from_activity(self, caen_activity):
        pass

    def _process_general_company_data(self, company, general_company_data, db):
        db.update_company_general_data(
            company.cif, general_company_data['response']['result'])
        db.update_company_available_balance_years(company.cif,
                                                  list(general_company_data['response']['result']['bilant']))
        db.update_company_status(company.cif,
            general_company_data['response']['result']['status']['stare'])

    def _process_balance_sheet_data(self, company, balance_sheet_year, balance_sheet_data, db):
        result = balance_sheet_data['response']['result']

        # Revenue
        revenue = result['cpp']['cifra_de_afaceri_neta'] if 'cifra_de_afaceri_neta' in result['cpp'] else 0

        # Profit
        profit_brut = 0
        profit_net = 0
        
        if 'profit' in result['cpp']:
            profit_brut = result['cpp']['profit']['brut'] if 'brut' in result['cpp']['profit'] else 0
            profit_net = result['cpp']['profit']['net'] if 'net' in result['cpp']['profit'] else 0

        # Number of employees
        number_of_employees = result['informatii']['salariati'] if 'salariati' in result['informatii'] else 0

        # CAEN activity
        caen_activity = result['informatii']['caen'] if 'caen' in result['informatii'] else ''

        db.update_company_activity(company.cif, caen_activity)

        # TODO: Determine CAEN code from CAEN activity
        caen_code = self._find_caen_code_from_activity(caen_activity)
        db.update_company_caen_code(company.cif, caen_code)

        self.number_of_companies_with_revenue += 1

        logger.info(
            f"[{self.number_of_companies_with_revenue}] - [{company.name} - {company.cif}] "
            f"Number of employees: {number_of_employees}, "
            f"Revenue: {revenue}, "
            f"Profit brut: {profit_brut}, "
            f"Profit net: {profit_net}, "
            f"CAEN: {caen_activity}, "
            f"Locality: {company.locality}, "
            f"MFinante URL: {company.mfinante_url}, "
            f"Total Firme URL: {company.total_firme_url}"
        )

        db.upsert_company_balance_sheet(
            company.cif, balance_sheet_year, result, revenue, profit_brut, profit_net, number_of_employees)

    def _process_company(self, company, db):
        if not company:
            return
        
        logger.info(
            f"[{company.number}/{self.number_of_companies}] Processing {company.name} - {company.cif}")

        # Step 1: Fetch general data for company
        general_company_data = self._fetch_general_company_data(company)
        if not general_company_data or general_company_data['response']['error']:
            logger.error(
                f"Cannot obtain general response for company: {company.name} - {company.cif}", CrawlerUtils.dump_json(general_company_data))
            return

        # Step 2: Process general data
        self._process_general_company_data(company, general_company_data, db)

        # Step 3: Determine latest year for balance sheet
        if len(general_company_data['response']['result']['bilant']) == 0:
            logger.info(
                f"Company: {company.name} ~ {company.cif} doesn't have any balance sheets!")
            return

        balance_sheet_year_str, balance_sheet_year = self._get_balance_sheet_year(
            company, general_company_data['response']['result']['bilant'])
        if not balance_sheet_year_str or not balance_sheet_year:
            logger.error(
                f"Cannot determine latest balance sheet year for company: {company.name} ~ {company.cif}!")
            return

        # Step 4: Fetch balance sheet data for company in given year
        balance_sheet_data = self._fetch_balance_sheet_data(company, balance_sheet_year_str)
        if not balance_sheet_data:
            logger.info(
                f"Company: {company.name} ~ ~ {company.cif} doesn't have balance in {balance_sheet_year_str}")
            return

        if balance_sheet_data['response']['error']:
            logger.warning(
                f"Cannot obtain balance sheet for company: {company.name} - {company.cif} in year: {balance_sheet_year_str}")
            CrawlerUtils.dump_json(balance_sheet_data)
            return
        
        if balance_sheet_data['response']['result']['declara_ca_nu_are_activitate']:
            declara_ca_nu_are_activitate = balance_sheet_data[
                'response']['result']['declara_ca_nu_are_activitate']
            logger.info(f"Company: {company.name} doesn't have balance in {self.fiscal_year} "
                        f"- [declara_ca_nu_are_activitate?: {declara_ca_nu_are_activitate}]")
            return
        
        # Step 5: Process balance sheet data
        self._process_balance_sheet_data(company, balance_sheet_year, balance_sheet_data, db)

    def _run(self, county, input_file):
        self.driver = CrawlerUtils.create_google_chrome_driver(MFinanteScraper.NAME)

        if not Path(input_file).is_file():
            logger.warning(f"No data for: {input_file}")
            return

        self.number_of_companies = CrawlerUtils.wc(input_file)
        logger.info(f"Number of companies to process: {self.number_of_companies} in file: {input_file}")

        # Input file
        logger.info(f"Input file: {input_file}")
        with open(input_file, encoding="utf-8") as input_csv_file:
            csv_reader = csv.reader(input_csv_file, delimiter='^')

            with Database(self.dsn) as db:
                start_row = None

                # Restore start_row value if any was previously saved
                semidb = semidbm.open(shelf_file, 'c')
                if input_file.encode() in semidb:
                    start_row = int(semidb[input_file])
                    logger.info("Start row should be: {}".format(start_row + 2))
                    del semidb[input_file]
                semidb.close()

                line_count = 0
                for row in csv_reader:
                    # Skip rows until the row we stopped in a previous run
                    if start_row and line_count <= start_row:
                        line_count += 1
                        continue
                    else:
                        line_count += 1

                    start_time = time.time()

                    try:
                        name = row[0]
                        cif = row[1]
                        mat = row[2]
                        address = row[5]
                        locality = CrawlerUtils.strip_diacritics(CrawlerUtils.parse_locality_from_address(address))
                        total_firme_url = CrawlerUtils.get_total_firme_url(name, cif)
                        mfinante_url = CrawlerUtils.get_mfinante_url(cif)

                        company = MFinanteScraper.Company(line_count, name, cif, mat, address, county, locality, mfinante_url, total_firme_url)
                        
                        self._process_company(company, db)

                        # Track row processing time
                        elapsed = time.time() - start_time
                        logger.info("{} Elapsed time: {} (s.ms)".format(line_count, elapsed))

                        # Backoff: take a nap after each row processed
                        if self.nap_time:
                            logger.info(
                                "Napping: {}s\n".format(self.nap_time))
                            time.sleep(self.nap_time)

                    except KeyboardInterrupt:
                        # NOTE: CTRL + C should only be called when napping
                        semidb = semidbm.open(shelf_file, 'c')
                        semidb[input_file] = str(line_count - 1)
                        semidb.close()
                        logger.info(
                            f"Saved last row before exit: {line_count}, Next row should be: {line_count - 1}")
                        exit(2)

                    logger.info(f'Processed {line_count} lines.')
                    CrawlerUtils.clear_shelve(shelf_file)
        
        # Stop web driver after processing has finished
        if self.driver:
            driver.quit()

    def test_run(self, fiscal_year, cif):
        name = 'Test MFinante'
        general_response = self.scrape_company_general_data(cif, True)

        if not general_response['response']['error']:
            if fiscal_year in general_response['response']['result']['bilant']:
                year = general_response['response']['result']['bilant'][fiscal_year]
                logger.info(f"Company has balance in {fiscal_year} - {year}")
                CrawlerUtils.dump_json(general_response)

                logger.info("\nGetting company balance sheet data")
                balance_sheet_response = self.scrape_company_balance_sheet_data(year, cif)
                CrawlerUtils.dump_json(balance_sheet_response)

                if balance_sheet_response:
                    result = balance_sheet_response['response']['result']

                    # Revenue
                    revenue = result['cpp']['cifra_de_afaceri_neta'] if 'cifra_de_afaceri_neta' in result['cpp'] else 0

                    # Profit
                    profit_brut = 0
                    profit_net = 0
                    if 'profit' in result['cpp']:
                        profit_brut = result['cpp']['profit']['brut'] if 'brut' in result['cpp']['profit'] else 0
                        profit_net = result['cpp']['profit']['net'] if 'net' in result['cpp']['profit'] else 0

                    # Number of employees
                    number_of_employees = result['informatii']['salariati'] if 'salariati' in result[
                        'informatii'] else 0

                    logger.info(
                        f"[{name} - {cif}] Number of employees: {number_of_employees}, Revenue: {revenue}, Profit brut: {profit_brut}, Profit net: {profit_net}")
            else:
                logger.info(f"Company doesn't have balance in {fiscal_year}")
        else:
            logger.info(CrawlerUtils.dump_json(general_response))
        CrawlerUtils.exit()
