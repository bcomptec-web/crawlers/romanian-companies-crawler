import logging
import argparse
import os
from romanian_companies.exporters import CompaniesByCAENExporter
from romanian_companies import CrawlerUtils

CrawlerUtils.configure_logger()
logger = logging.getLogger(__name__)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Exporter of companies with given CAEN codes')
    parser.add_argument('--output_dir', type=str, required=True, help='the directory where to store output (mandatory)')
    parser.add_argument('--fiscal_year', metavar='fiscal_year', type=int, default=0,
                        help='the fiscal year to fetch companies only with balance in specified year (optional)')
    parser.add_argument('--caen_codes', metavar='caen_codes', type=str, required=True, nargs='+',
                        help='the list of CAEN codes to get data for (mandatory)')
    args = parser.parse_args()

    if not args.caen_codes or not args.output_dir:
        parser.print_usage()
        CrawlerUtils.exit(1)

    try:
        dsn = os.environ['DSN']
    except KeyError as err:
        logger.error("Missing DSN environment variable")
        CrawlerUtils.exit(1)

    caen_codes = [int(s.strip()) for s in args.caen_codes]

    exporter = CompaniesByCAENExporter(dsn, args.output_dir, caen_codes, args.fiscal_year)
    exporter.run()
