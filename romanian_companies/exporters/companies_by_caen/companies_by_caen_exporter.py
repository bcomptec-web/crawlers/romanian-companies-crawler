import logging
import csv
import os
import xlsxwriter
from PIL import ImageFont
from romanian_companies import CrawlerUtils
from romanian_companies.database import Database

logger = logging.getLogger(__name__)


class CompaniesByCAENExporter:
    def __init__(self, dsn, output_dir, caen_codes, fiscal_year):
        self.dsn = dsn
        self.output_dir = output_dir
        self.caen_codes = caen_codes
        self.fiscal_year = fiscal_year
        self.font_name = 'Times New Roman'
        self.font_file = '/usr/share/fonts/TTF/times.ttf'
        self.font_size = 12

        """
        In case of:
        munmap_chunk(): invalid pointer
        [1]    733649 abort (core dumped)

        corrupted size vs. prev_size
        [1]    735870 abort (core dumped)
        please see: https://github.com/python-pillow/Pillow/issues/4363#issuecomment-575406405 for a solution
        """
        self.font = ImageFont.truetype(self.font_file, size=self.font_size)

    def run(self):
        logger.info(f"Output Dir: {self.output_dir}, CAEN codes: {self.caen_codes}, Fiscal Year: {self.fiscal_year}")

        if not self.output_dir or not self.caen_codes:
            logger.error("Missing output dir or CAEN codes")
            return

        if not os.path.exists(self.output_dir):
            os.makedirs(self.output_dir)
        
        output_filename = "Romanian_Companies_Brasov_August_2020.xlsx"
        if self.fiscal_year:
            output_filename = f"Romanian_Companies_Brasov_August_2020_With_Last_Balance_In_{self.fiscal_year}.xlsx"
        
        output_file = os.path.join(self.output_dir, output_filename)
        if os.path.exists(output_file):
            os.remove(output_file)
        workbook = xlsxwriter.Workbook(output_file)
        
        cell_format = workbook.add_format(
            {'font_name': self.font_name, 'font_size': self.font_size, 'text_wrap': True, 'align': 'center', 'valign': 'vcenter'})

        try:
            with Database(self.dsn) as db:
                for caen_code in self.caen_codes:
                    activity = db.get_activity_by_caen_code(caen_code)
                    activity_normalized = (activity[:23].strip() + '...') if len(activity) > 23 else activity

                    worksheet = workbook.add_worksheet(f"{caen_code}-{activity_normalized}")

                    outdata = []

                    # Header
                    self._write_header(workbook, worksheet, outdata)

                    # Body
                    companies = self._get_companies(db, caen_code, self.fiscal_year)

                    self._write_data(companies, worksheet, cell_format, outdata)

                    self._autofit_column_widths(worksheet, outdata)
        except Exception as ex:
            logger.error(CrawlerUtils.exception_to_string(ex))
        finally:
            workbook.close() 

    def _get_companies(self, db, caen_code, fiscal_year):
        companies = None
        if fiscal_year: # a.cui, nume, cod_inmatriculare, judet, localitate, cod_caen, activitate, total_firme_url, mfinante_url, b.cifra_de_afaceri, b.profit_brut, b.profit_net, b.numar_angajati
            companies = db.get_companies_by_caen_code_with_balance_in_year(caen_code, fiscal_year)
        else: # cui, nume, cod_inmatriculare, judet, localitate, cod_caen, activitate, total_firme_url, mfinante_url
            companies = db.get_companies_by_caen_code(caen_code)
        return companies

    def _write_header_all_companies(self, workbook, worksheet, outdata):
        header_data = ['Name', 'TIN', 'Registration Number', 'Locality', 'Total Firme URL', 'Ministry of Finante URL']
        outdata.append(header_data)

        worksheet.freeze_panes(1, 0)

        header_format = workbook.add_format({'bold': True,
                                             'bottom': 2,
                                             'bg_color': '#F9DA04',
                                             'text_wrap': True,
                                             'align': 'center',
                                             'valign': 'vcenter'})

        for col_num, data in enumerate(header_data):
            worksheet.write(0, col_num, data, header_format)

    
    def _write_header_companies_with_balance(self, workbook, worksheet, outdata):
        header_data = ['Name', 'TIN', 'Registration Number', 'Turnover', 'Gross Profit', 'Net Profit', 'Employees', 'Locality', 'Total Firme URL', 'Ministry of Finante URL']
        outdata.append(header_data)

        worksheet.freeze_panes(1, 0)

        header_format = workbook.add_format({'bold': True,
                                             'bottom': 2,
                                             'bg_color': '#F9DA04',
                                             'text_wrap': True,
                                             'align': 'center',
                                             'valign': 'vcenter'})

        for col_num, data in enumerate(header_data):
            worksheet.write(0, col_num, data, header_format)

    def _write_header(self, workbook, worksheet, outdata):
        worksheet.set_row(0, 30)

        if self.fiscal_year:
            self._write_header_companies_with_balance(workbook, worksheet, outdata)
        else:
            self._write_header_all_companies(workbook, worksheet, outdata)

    def _autofit_column_widths(self, worksheet, data):
        """
        Autofit column widths based on the column contents
        """
        if not data:
            return
        columns_width = self._get_col_width(data, self.font_file, self.font_size)
        self._set_col_width(worksheet, columns_width)

    def _get_cell_size(self, value, font, dimension="width"):
        """ value: cell content
            font_name: The name of the font in the target cell
            font_size: The size of the font in the target cell """        
        (size, h) = font.getsize(str(value))
        if dimension == "height":
            return size * 0.92  # fit value experimentally determined
        return size * 0.20  # fit value experimentally determined

    def _get_col_width(self, data, font_name, font_size, min_width=1):
        """ Assume 'data' to be an iterable (rows) of iterables (columns / cells)
        Also, every cell is assumed to have the same font and font size.
        Returns a list with the autofit-width per column """
        colwidth = [min_width for col in data[0]]        
        for x, row in enumerate(data):
            for y, value in enumerate(row):
                colwidth[y] = max(colwidth[y], self._get_cell_size(value, self.font))
        return colwidth

    def _set_col_width(self, worksheet, columns):
        for column_idx, column_width in enumerate(columns):
            # logger.info(f"{worksheet.get_name()} Setting width to {column_width} for column {column_idx}")
            worksheet.set_column(column_idx, column_idx, column_width)

    def _write_company(self, company, cell_format, row, worksheet, outdata):
        nume_column = 'A'
        cui_column = 'B'
        cod_inmatriculare_column = 'C'
        localitate_column = 'D'
        total_firme_column = 'E'
        mfinante_url_column = 'F'

        cui, nume, cod_inmatriculare, judet, localitate, cod_caen, activitate, total_firme_url, mfinante_url = company

        worksheet.write(f"{nume_column}{row}", nume, cell_format)
        worksheet.write(f"{cui_column}{row}", cui, cell_format)
        worksheet.write(f"{cod_inmatriculare_column}{row}", cod_inmatriculare, cell_format)
        worksheet.write(f"{localitate_column}{row}", localitate, cell_format)
        worksheet.write(f"{total_firme_column}{row}", total_firme_url, cell_format)
        worksheet.write(f"{mfinante_url_column}{row}", mfinante_url, cell_format)

        outdata.append([nume, cui, cod_inmatriculare, localitate, total_firme_url, mfinante_url])

    def _write_company_with_balance(self, company, cell_format, row, worksheet, outdata):
        cui, nume, cod_inmatriculare, judet, localitate, cod_caen, activitate, total_firme_url, mfinante_url, cifra_de_afaceri, profit_brut, profit_net, numar_angajati = company

        nume_column = 'A'
        cui_column = 'B'
        cod_inmatriculare_column = 'C'
        cifra_de_afaceri_column = 'D'
        profit_brut_column = 'E'
        profit_net_column = 'F'
        numar_angajati_column = 'G'
        localitate_column = 'H'
        total_firme_column = 'I'
        mfinante_url_column = 'J'

        worksheet.write(f"{nume_column}{row}", nume, cell_format)
        worksheet.write(f"{cui_column}{row}", cui, cell_format)
        worksheet.write(f"{cod_inmatriculare_column}{row}", cod_inmatriculare, cell_format)

        # Balance data
        worksheet.write(f"{cifra_de_afaceri_column}{row}", cifra_de_afaceri, cell_format)
        worksheet.write(f"{profit_brut_column}{row}", profit_brut, cell_format)
        worksheet.write(f"{profit_net_column}{row}", profit_net, cell_format)
        worksheet.write(f"{numar_angajati_column}{row}", numar_angajati, cell_format)

        worksheet.write(f"{localitate_column}{row}", localitate, cell_format)
        worksheet.write(f"{total_firme_column}{row}", total_firme_url, cell_format)
        worksheet.write(f"{mfinante_url_column}{row}", mfinante_url, cell_format)

        outdata.append([nume, cui, cod_inmatriculare, cifra_de_afaceri, profit_brut, profit_net, numar_angajati, localitate, total_firme_url, mfinante_url])

    def _write_data(self, companies, worksheet, cell_format, outdata):
        row = 2
        for company in companies:
            if self.fiscal_year:
                self._write_company_with_balance(company, cell_format, row, worksheet, outdata)
            else:
                self._write_company(company, cell_format, row, worksheet, outdata)
            row += 1