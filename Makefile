.PHONY: mfinante onrc tests help sync

SHELL := /bin/bash

mfinante:
## mfinante: Run MFinante crawler, e.g. make INPUT_FILE=Data/companies_per_county/Brasov/xaa.csv DSN="postgresql://romanian_companies:fln8nv6hfxMoXYRDiNWx@localhost:5432/romanian_companies" mfinante
	DSN="postgresql://romanian_companies:fln8nv6hfxMoXYRDiNWx@localhost:5432/romanian_companies" python -m romanian_companies.crawlers.mfinante --sleep_time 2 --counties Brasov --input_file ${INPUT_FILE}

totalfirme:
## totalfirme: Run TotalFirme crawler, e.g. make INPUT_FILE=Data/companies_per_county/Brasov/xaa.csv DSN="postgresql://romanian_companies:fln8nv6hfxMoXYRDiNWx@localhost:5432/romanian_companies" totalfirme
	DSN="postgresql://romanian_companies:fln8nv6hfxMoXYRDiNWx@localhost:5432/romanian_companies" python -m romanian_companies.crawlers.total_firme --sleep_time 2 --counties Brasov --input_file ${INPUT_FILE}
	
onrc:
## onrc: Run ONRC data importer and create database schema if not exists, e.g. make COUNTIES="Brasov" DSN="postgresql://romanian_companies:fln8nv6hfxMoXYRDiNWx@localhost:5432/romanian_companies" onrc
	python -m romanian_companies.importers.onrc $(COUNTIES)

caen_import:
## caen: Run CAEN data importer and create database schema if not exists, e.g. make INPUT_FILE=Data/caen/caen-clase-rev2.csv DSN="postgresql://romanian_companies:fln8nv6hfxMoXYRDiNWx@localhost:5432/romanian_companies" caen_import
	python -m romanian_companies.importers.caen.importer ${INPUT_FILE}
	
caen_process:
## caen: Run CAEN data processor, e.g. make DSN="postgresql://romanian_companies:fln8nv6hfxMoXYRDiNWx@localhost:5432/romanian_companies" caen_process
	python -m romanian_companies.importers.caen.processor

FISCAL_YEAR ?= 0
companies_by_caen_exporter:
## caen: Run CAEN companies by CAEN exporter, e.g. make DSN="postgresql://romanian_companies:fln8nv6hfxMoXYRDiNWx@localhost:5432/romanian_companies" CAEN_CODES="4120 4941 5610 6820 5590 4321 4110 5510 3109 7111 7410 4759 3101 4329 3102 4647 5210" FISCAL_YEAR=2019 OUTPUT_DIR=$(pwd)/export companies_by_caen_exporter
	python -m romanian_companies.exporters.companies_by_caen --output_dir $(OUTPUT_DIR) --caen_codes $(CAEN_CODES) --fiscal_year $(FISCAL_YEAR)
	
tests:
## tests: Run all tests
	python -m unittest -v romanian_companies/test_crawler_utils.py


help:
## help: Show list of commands
	@echo "Usage:"
	@sed -n 's/^##//p' ${MAKEFILE_LIST} | column -t -s ':' | sed -e 's/^/-/'

docker_network:
	docker network create romanian_companies_network -d bridge || true
	
docker_image:	
	docker build $(DOCKER_BUILD_OPTS) -f ./Dockerfile -t romanian_companies/x86-alpine-romanian-companies-crawler:latest -t romanian_companies/x86-alpine-romanian-companies-crawler:0.0.1 .

start:
	docker-compose -f docker-compose.yml up -d

stop:
	docker-compose -f docker-compose.yml stop
	
start_db:
	docker-compose -f docker-compose.yml up -d romanian-companies-db
	sleep 3
	
stop_db:
	docker-compose -f docker-compose.yml stop romanian-companies-db
	
drop_db:
	docker-compose -f docker-compose.yml rm -f -s -v romanian-companies-db

dump_db:
	PGPASSWORD=fln8nv6hfxMoXYRDiNWx pg_dump --verbose --host=localhost --port=5432 --username=romanian_companies --format=p --compress=9 --encoding=UTF-8 -n public romanian_companies > "database/dumps/dump-romanian_companies-$(shell date "+%d_%m_%Y_%H_%M_%S").sql"

psql:
	@docker exec -it $$(docker ps | grep romanian-companies-db | awk '{print $$12}') psql -U romanian_companies
	
export_csv:
	@PGPASSWORD=fln8nv6hfxMoXYRDiNWx psql -U romanian_companies -h localhost -d romanian_companies -c "\copy (SELECT nume,cui,cod_inmatriculare,euid,stare_onrc,adresa FROM firme WHERE activitate is NULL or activitate = '') TO 'firme.csv' DELIMITER '^' CSV"
	
sync:
	DSN="postgresql://romanian_companies:fln8nv6hfxMoXYRDiNWx@localhost:5432/romanian_companies" python -m romanian_companies.importers.data_sync

show_progress:
	@PGPASSWORD=fln8nv6hfxMoXYRDiNWx psql -q -U romanian_companies -h localhost -d romanian_companies -f Data/show_progress.sql | less
