#!/bin/bash

# If app-pre-stop.sh exists, run it
if [[ -x /scripts/pre-stop.sh ]]
then
   echo "Executing /scripts/app-pre-stop.sh"
   /scripts/app-pre-stop.sh "$@"
   echo "/scripts/app-pre-stop.sh returned $?"
fi

echo "Sending kill signal to app-daemon"
PID=$(pidof python)
kill $PID

# Give app-daemon time to shut down
for i in {1..10}; do
    ps -p $PID &> /dev/null || break
    sleep .2
done

# If app-post-stop.sh exists, run it
if [[ -x /scripts/app-post-stop.sh ]]
then
   echo "Executing /scripts/app-post-stop.sh"
   /scripts/app-post-stop.sh "$@"
   echo "/scripts/app-post-stop.sh returned $?"
fi
