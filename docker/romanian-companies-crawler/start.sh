#!/bin/bash

# Source our persisted env variables from container startup
. /etc/romanian-companies-crawler/environment-variables.sh

# This script will be called with tun/tap device name as parameter 1, and local IP as parameter 4
# See https://openvpn.net/index.php/open-source/documentation/manuals/65-openvpn-20x-manpage.html (--up cmd)
echo "Up script executed with $*"
if [[ "$4" = "" ]]; then
   echo "ERROR, unable to obtain tunnel address"
   echo "killing $PPID"
   kill -9 $PPID
   exit 1
fi

# If app-pre-start.sh exists, run it
if [[ -x /scripts/app-pre-start.sh ]]
then
   echo "Executing /scripts/app-pre-start.sh"
   /scripts/app-pre-start.sh "$@"
   echo "/scripts/app-pre-start.sh returned $?"
fi

if [[ ! -e "/dev/random" ]]; then
  # Avoid "Fatal: no entropy gathering module detected" error
  echo "INFO: /dev/random not found - symlink to /dev/urandom"
  ln -s /dev/urandom /dev/random
fi

. /etc/romanian-companies-crawler/userSetup.sh

if [[ "true" = "$DROP_DEFAULT_ROUTE" ]]; then
  echo "DROPPING DEFAULT ROUTE"
  ip r del default || exit 1
fi

if [[ "true" = "$LOG_TO_STDOUT" ]]; then
  LOGFILE=/dev/stdout
else
  LOGFILE=${ROMANIAN_COMPANIES_CRAWLER_HOME}/app.log
fi

echo "STARTING ROMANIAN COMPANIES CRAWLER"

echo -e "ROMANIAN_COMPANIES_CRAWLER_SCRIPT_NAME=${ROMANIAN_COMPANIES_CRAWLER_SCRIPT_NAME}\nROMANIAN_COMPANIES_CRAWLER_SLEEP_TIME=${ROMANIAN_COMPANIES_CRAWLER_SLEEP_TIME}\nROMANIAN_COMPANIES_CRAWLER_COUNTIES=${ROMANIAN_COMPANIES_CRAWLER_COUNTIES}\n"

exec su --preserve-environment ${RUN_AS} -s /bin/bash -c "/go/bin/retry -timeout 525948m -retry-interval 10s /usr/local/bin/python -m ${ROMANIAN_COMPANIES_CRAWLER_SCRIPT_NAME} --sleep_time ${ROMANIAN_COMPANIES_CRAWLER_SLEEP_TIME} --counties ${ROMANIAN_COMPANIES_CRAWLER_COUNTIES} --logfile $LOGFILE" &

# If app-post-start.sh exists, run it
if [[ -x /scripts/app-post-start.sh ]]
then
   echo "Executing /scripts/app-post-start.sh"
   /scripts/app-post-start.sh "$@"
   echo "/scripts/app-post-start.sh returned $?"
fi

echo "App startup script complete."
