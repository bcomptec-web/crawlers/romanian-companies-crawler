### Required environment options
| Variable           | Function                          | Example                                                                                                 |
| ------------------ | --------------------------------- | ------------------------------------------------------------------------------------------------------- |
| `OPENVPN_PROVIDER` | Sets the OpenVPN provider to use. | `OPENVPN_PROVIDER=provider`. Supported providers and their config values are listed in the table above. |
| `OPENVPN_USERNAME` | Your OpenVPN username             | `OPENVPN_USERNAME=asdf`                                                                                 |
| `OPENVPN_PASSWORD` | Your OpenVPN password             | `OPENVPN_PASSWORD=asdf`                                                                                 |

### Network configuration options
| Variable            | Function                                                                                            | Example                                                                                                        |
| ------------------- | --------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------- |
| `OPENVPN_CONFIG`    | Sets the OpenVPN endpoint to connect to.                                                            | `OPENVPN_CONFIG=UK Southampton`                                                                                |
| `OPENVPN_OPTS`      | Will be passed to OpenVPN on startup                                                                | See [OpenVPN doc](https://openvpn.net/index.php/open-source/documentation/manuals/65-openvpn-20x-manpage.html) |
| `LOCAL_NETWORK`     | Sets the local network that should have access. Accepts comma separated list.                       | `LOCAL_NETWORK=192.168.0.0/24`                                                                                 |
| `CREATE_TUN_DEVICE` | Creates /dev/net/tun device inside the container, mitigates the need mount the device from the host | `CREATE_TUN_DEVICE=true`                                                                                       |

### Health check option

Because your VPN connection can sometimes fail, Docker will run a health check on this container every 5 minutes to see if the container is still connected to the internet. By default, this check is done by pinging google.com once. You change the host that is pinged.

| Variable            | Function                                                           | Example      |
| ------------------- | ------------------------------------------------------------------ | ------------ |
| `HEALTH_CHECK_HOST` | this host is pinged to check if the network connection still works | `google.com` |

### Crawler configuration options

You may override app options by setting the appropriate environment variable.


| Variable                   | Function                              |
| -------------------------- | ------------------------------------- |
TODO

As you can see the variables are prefixed with `ROMANIAN_COMPANIES_CRAWLER_`, the variable is capitalized, and `-` is converted to `_`.

### Web proxy configuration options

This container also contains a web-proxy server to allow you to tunnel your web-browser traffic through the same OpenVPN tunnel.
This is useful if you are using a private tracker that needs to see you login from the same IP address you are torrenting from.
The default listening port is 8888. Note that only ports above 1024 can be specified as all ports below 1024 are privileged
and would otherwise require root permissions to run.
Remember to add a port binding for your selected (or default) port when starting the container.
If you set Username and Password it will enable BasicAuth for the proxy

| Variable           | Function                | Example                 |
| ------------------ | ----------------------- | ----------------------- |
| `WEBPROXY_ENABLED` | Enables the web proxy   | `WEBPROXY_ENABLED=true` |
| `WEBPROXY_PORT`    | Sets the listening port | `WEBPROXY_PORT=8888`    |
| `WEBPROXY_USERNAME`| Sets the BasicAuth username | `WEBPROXY_USERNAME=test`    |
| `WEBPROXY_PASSWORD`| Sets the BasicAuth password  | `WEBPROXY_PASSWORD=password`    |

### User configuration options

By default everything will run as the root user. However, it is possible to change who runs the app process.
You may set the following parameters to customize the user id that runs the app.

| Variable | Function                                    | Example     |
| -------- | ------------------------------------------- | ----------- |
| `PUID`   | Sets the user id who will run the app  | `PUID=1003` |
| `PGID`   | Sets the group id for the app user | `PGID=1003` |

### Dropping default route from iptables (advanced)

Some VPNs do not override the default route, but rather set other routes with a lower metric.
This might lead to the default route (your untunneled connection) to be used.

To drop the default route set the environment variable `DROP_DEFAULT_ROUTE` to `true`.

*Note*: This is not compatible with all VPNs. You can check your iptables routing with the `ip r` command in a running container.

### Changing logging locations

By default the app will log to a file in `ROMANIAN_COMPANIES_CRAWLER_HOME/app.log`. 

To log to stdout instead set the environment variable `LOG_TO_STDOUT` to `true`.

*Note*: By default stdout is what container engines read logs from. Set this to true to have Tranmission logs in commands like `docker logs` and `kubectl logs`.  
OpenVPN currently only logs to stdout.  
