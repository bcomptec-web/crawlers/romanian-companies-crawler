#!/usr/bin/env bash

# ./run_container.sh "6201,6202,6203,5829,6209,6311,6312,7112" "Bucuresti" "Ilfov" "Cluj" "Iasi" "Timis" "Brasov" "Sibiu" "Mures" "Bihor" "Arad" "Dolj" "Alba" "Constanta" "Arges" "Bacau" "Bistrita-nasaud" "Botosani" "Braila" "Buzau" "Calarasi" "Caras-severin" "Covasna" "Dambovita" "Galati" "Giurgiu" "Gorj" "Harghita" "Hunedoara" "Ialomita" "Maramures" "Mehedinti" "Neamt" "Olt" "Prahova" "Salaj" "Satu-mare" "Suceava" "Teleorman" "Tulcea" "Valcea" "Vaslui" "Vrancea"

docker run --rm --name mfinante -v $(pwd)/docker_output:/output py3.7-alpine3.8-selenium:latest "$@"
 
