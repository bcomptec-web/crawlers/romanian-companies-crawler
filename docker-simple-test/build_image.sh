#!/usr/bin/env bash

NAME="py3.7-alpine3.8-selenium"
TAG="latest"

docker build -t "$NAME:$TAG" .

docker images | grep "$NAME"
