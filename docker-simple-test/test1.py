from selenium import webdriver
from selenium.webdriver.chrome.options import Options 

options = webdriver.ChromeOptions()
options.add_argument("--no-sandbox")
options.add_argument('--headless')
options.add_argument('--disable-dev-shm-usage')
options.add_argument("--disable-extensions")
options.add_argument('--disable-gpu')
options.binary_location = '/usr/bin/chromium-browser'
driver = webdriver.Chrome(executable_path='/usr/bin/chromedriver', chrome_options=options, service_args=['--verbose', "--log-path=/output/chromedriver.log"])

driver.get("https://httpstat.us/200")

if "200 OK" in driver.page_source:
    print('Selenium successfully opened with Chrome and navigated to "https://httpstat.us/200", you\'re all set!')
