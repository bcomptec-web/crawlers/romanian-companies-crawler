#!/bin/sh

chromium-browser --headless --no-sandbox --disable-dev-shm-usage --disable-extensions --disable-gpu --version
chromedriver --version

echo "Args: $@"

python3 test1.py
