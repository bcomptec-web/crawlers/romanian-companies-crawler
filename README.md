# romanian-companies-crawler
Romanian companies web crawling using datasets gathered from multiple sources ([data.gov.ro](https://data.gov.ro/), [mfinante.gov.ro](https://www.mfinante.gov.ro/pagina.html?pagina=acasa), [termene.ro](https://termene.ro), [totalfirme.ro](https://www.totalfirme.ro/)).

## Requirements
- Python 3 (See `requirements.txt`)
  - Install requirements: `pip install --user -r requirements.txt`  
- Bash
- XPath
- Selenium WebDriver
- Headless Chrome Driver
    - Download from https://sites.google.com/a/chromium.org/chromedriver/downloads for the Chrome version used and put it in root directory with name `chromedriver`
- Xvfb virtual display
- Xidel
- cURL
- Golang retry command wrapper
- Docker (Alpine Linux)

## Structure
- Input  
    - `split_data_gov_companies_per_county.sh`: split/aggregate all romanian companies per county using data from ONRC available from [data.gov.ro](https://data.gov.ro/organization/onrc)  
    - `get_input.sh`: Get romanian companies for specified NACE codes from termene.ro  
    - `get_input_in_parallel.sh`: Alternative to `get_input.sh`, which gets input in parallel for multiple counties at the same time  
    - `count_input.sh`: count the number of companies per NACE code specified in each county  
    - `check_links.sh`: validate generated URLs for existence  
    - `check_files.sh`: check differences between previous and current datasets  
    - `count_input.sh`: count the number of companies in each county and for each caen code  
    - `grep.sh`: search for company in input files  
    - `run_mfinante.sh`: run mfinante crawler with retry mechanism  
    - `run_total_firme.sh`: run total firme crawler with retry mechanism  
- Importers:  
    - `onrc_importer`: Import data from ONRC (downloaded from data.gov.ro) into SQLite  
- Crawlers:  
    - `mfinante_crawler.py`: Crawling data from mfinante.ro  
    - `total_firme_crawler.py`: Crawling data from totalfirme.ro  
- Output:  
    - `count_output.sh`: count number of companies in crawler output files  
    - `aggregate-all.sh`: create a single file with the companies of a county from all the CAEN code files  
    - `git-add-all.sh`: add all datasets to the Git repository  
    - `post_process.sh`: process crawler output files and create a zip archive with companies data of each county  

## Usage

### CLI
1. Download input data from data.gov.ro
2. Run `companies_per_county.sh` to process the input data
3. Run `get_input.sh` or `get_input_in_parallel.sh` (optional, get input data from termene.ro)
4. Run `onrc_importer.py` to import input data into SQLite database.

```
$ python -m romanian_companies.importers.onrc
usage: __main__.py [-h] [counties [counties ...]]

Importer for ONRC data from data.gov.ro

positional arguments:
  counties    the list of counties to import data for (mandatory)

optional arguments:
  -h, --help  show this help message and exit
```

5. Run `total_firme_crawler.py`
```
 $ python -m romanian_companies.crawlers.total_firme
usage: __main__.py [-h] [--sleep_time sleep_time] [--fiscal_year fiscal_year] [--counties counties [counties ...]]

Crawler for total firme.

optional arguments:
  -h, --help            show this help message and exit
  --sleep_time sleep_time
                        the sleep time in seconds between requests (optional)
  --fiscal_year fiscal_year
                        the fiscal year to fetch bilant for (optional)
  --counties counties [counties ...]
                        the list of counties to get data for (mandatory)
```

6. Run `mfinante_crawler.py`
```
$ python -m romanian_companies.crawlers.mfinante
usage: __main__.py [-h] [--sleep_time sleep_time] [--fiscal_year fiscal_year] [--counties counties [counties ...]] [--caen_codes [caen_codes [caen_codes ...]]]

Crawler for mfinante.ro

optional arguments:
  -h, --help            show this help message and exit
  --sleep_time sleep_time
                        the sleep time in seconds between requests (optional)
  --fiscal_year fiscal_year
                        the fiscal year to fetch bilant for (optional)
  --counties counties [counties ...]
                        the list of counties to get data for (mandatory)
  --caen_codes [caen_codes [caen_codes ...]]
                        the list of CAEN codes to get data for (optional)
```

7. Run `post_process.sh`
8. View output in `Romanian_Companies_Financial_Data_Output`

### Code (Python package import)
```python
from romanian_companies import MFinanteScraper, TotalFirmeScraper, ONRCImporter
```

## Retry
- [Download Golang](https://golang.org/dl/)
- Install [retry package](https://github.com/mh-cbon/retry): `go get github.com/mh-cbon/retry`
- Run: `retry -timeout 525948m -retry-interval 10s python mfinante.py <params>`

## Docker

## Simple (see `docker-simple-test` directory)
- Build image: `build_image.sh` (See `Dockerfile` for details)
- Entrypoint: `docker_entrypoint.sh`
- Run container: `run_container.sh`

## VPN
```
docker stop x86-alpine-romanian-companies-crawler
docker rm x86-alpine-romanian-companies-crawler
docker rmi x86-alpine-romanian-companies-crawler

-- make image
docker build --no-cache --tag x86-alpine-romanian-companies-crawler:latest .

-- run container for single county with random VPN config
docker run --cap-add=NET_ADMIN -d \
    --name x86-alpine-romanian-companies-crawler \
    -v $(pwd)/docker_data:/data \
    -v $(pwd)/Data:/opt/app/Data \
    -v /etc/localtime:/etc/localtime \
    -e OPENVPN_PROVIDER=PROTONVPN \
    -e OPENVPN_CONFIG=$(find docker/openvpn/protonvpn -type f -name *.ovpn | shuf -n 1 | xargs -I{} basename {} .ovpn) \
    -e OPENVPN_USERNAME=<username> \
    -e OPENVPN_PASSWORD=<password> \
    -e WEBPROXY_ENABLED=false \
    -e LOCAL_NETWORK=192.168.0.0/16 \
    -e ROMANIAN_COMPANIES_CRAWLER_SCRIPT_NAME=mfinante_onrc.py \
    -e ROMANIAN_COMPANIES_CRAWLER_SLEEP_TIME=30 \
    -e ROMANIAN_COMPANIES_CRAWLER_COUNTIES="Vrancea" \
    --log-driver json-file \
    --log-opt max-size=10m \
   x86-alpine-romanian-companies-crawler
    
docker logs -tf x86-alpine-romanian-companies-crawler
```

## Data points
MFinante General:  
```
0: 'Denumire platitor',
1: 'Adresa',
2: 'Judetul',
3: 'Numar de inmatriculare la Registrul Comertului',
4: 'Act autorizare',
5: 'Codul postal',
6: 'Telefon',
7: 'Fax',
8: 'Stare societate',
9: 'Observatii privind societatea comerciala',
10: 'Data inregistrarii ultimei declaratii',
11: 'Data ultimei prelucrari',
12: 'Impozit pe profit (data luarii in evidenta)',
13: 'Impozit pe veniturile microintreprinderilor (data luarii in evidenta)',
14: 'Accize (data luarii in evidenta)',
15: 'Taxa pe valoarea adaugata (data luarii in evidenta)',
16: 'Contributiile de asigurari sociale (data luarii in evidenta)',
17: 'Contributia asiguratorie pentru munca (data luarii in evidenta)',
18: 'Contributia de asigurari sociale de sanatate(data luarii in evidenta)',
19: 'Taxa jocuri de noroc (data luarii in evidenta)',
20: 'Impozit pe veniturile din salarii si asimilate salariilor (data luarii in evidenta)',
21: 'Impozit pe constructii(data luarii in evidenta)',
22: 'Impozit la titeiul si la gazele naturale din productia interna (data luarii in evidenta)',
23: 'Redevente miniere/Venituri din concesiuni si inchirieri (data luarii in evidenta)',
24: 'Redevente petroliere (data luarii in evidenta)',
```

MFinante Bilant:  
```
// Indicatori din BILANT 
0: 'Active imobilizate total',
1: 'Active circulante total',
2: 'Stocuri (materii prime, materiale, productie in curs de executie, semifabricate, produse finite, marfuri etc.)',
3: 'Creante',
4: 'Casa si conturi la banci',
5: 'Cheltuieli in avans',
6: 'Datorii',
7: 'Venituri in avans',
8: 'Provizioane',

// Capitaluri
9: 'Capitaluri total',
10: 'Capital subscris varsat (Capital social)',
11: 'Patrimoniul regiei',
12: 'Patrimoniul public',

// Indicatori din CONTUL DE PROFIT SI PIERDERE 
13: 'Cifra de afaceri neta',
14: 'Venituri total',
15: 'Cheltuieli total',
16: 'Profit brut',
17: 'Pierdere bruta',
18: 'Profit net',
19: 'Pierdere neta',

// Indicatori din DATE INFORMATIVE 
20: 'Numar mediu de salariati',
21: 'Tipul de activitate, conform clasificarii CAEN',
```

ONRC:
```
  0: 'Denumire'
  1: 'CUI'
  2: 'Cod inmatriculare'
  3: 'EUID'
  4: 'Stare firma'
  5: 'Adresa'
```

## Tips
- When not running in headless mode, prevent window from showing up: "KDE: Move window to virtual desktop" https://askubuntu.com/questions/404639/automatic-sending-of-windows-to-virtual-desktop-2-in-plasma