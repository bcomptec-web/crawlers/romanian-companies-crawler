#!/usr/bin/env python

import csv
import sys
from xlsxwriter.workbook import Workbook
from PIL import ImageFont
import multiprocessing
from concurrent.futures.thread import ThreadPoolExecutor


# https://xlsxwriter.readthedocs.io/


def get_cell_size(value, font_name, font_size, dimension="width"):
    """ value: cell content
        font_name: The name of the font in the target cell
        font_size: The size of the font in the target cell """
    font = ImageFont.truetype(font_name, size=font_size)
    (size, h) = font.getsize(str(value))
    if dimension == "height":
        return size * 0.92  # fit value experimentally determined
    return size * 0.20  # fit value experimentally determined


def get_col_width(data, font_name, font_size, min_width=1):
    """ Assume 'data' to be an iterable (rows) of iterables (columns / cells)
    Also, every cell is assumed to have the same font and font size.
    Returns a list with the autofit-width per column """
    colwidth = [min_width for col in data[0]]
    for x, row in enumerate(data):
        for y, value in enumerate(row):
            colwidth[y] = max(colwidth[y], get_cell_size(value, font_name, font_size))
    return colwidth


def convert_to_xlsx(csv_file, font_name, font_file, font_size):
    # print(f"Converting {csv_file} to XLSX", flush=True)
    output_xlsx_filename = csv_file[:-4] + '.xlsx'
    workbook = Workbook(output_xlsx_filename)
    cell_format = workbook.add_format(
        {'font_name': font_name, 'font_size': font_size})  # 'align': 'center', 'valign': 'vcenter'

    worksheet = workbook.add_worksheet()

    try:
        with open(csv_file, 'r', encoding='UTF-8') as f:
            reader = csv.reader(f, delimiter='\t')

            data = list(reader)

            for rowIndex, row in enumerate(data):
                for columnIndex, column in enumerate(row):
                    # print(f"Writing at cell: {rowIndex}{columnIndex} data: {column}", flush=True)
                    worksheet.write(rowIndex, columnIndex, column, cell_format)

            # Autofit column widths based on the column contents
            columnWidths = get_col_width(data, font_file, font_size)
            for columnIndex, width in enumerate(columnWidths):
                # print(f"Setting column {columnIndex} to width: {width}", flush=True)
                worksheet.set_column(columnIndex, columnIndex, width)
    except Exception as ex:
        print(f"Error while converting {csv_file}: {ex}", flush=True)
    finally:
        print(output_xlsx_filename, flush=True)
        workbook.close()


# https://xlsxwriter.readthedocs.io/format.html
# The default width is 8.43 in the default font of Calibri 11.
# Excel can only display fonts that are installed on the system that it is running on. 
# Therefore it is best to use the fonts that come as standard such as ‘Calibri’, ‘Times New Roman’ and ‘Courier New’.
# The default font for an unformatted cell in Excel 2007+ is ‘Calibri’.
if __name__ == '__main__':
    if not len(sys.argv) > 1:
        print("Please provide CSV files")
        sys.exit(1)

    csv_files = sys.argv[1:]
    font_name = 'Times New Roman'
    font_file = '/usr/share/fonts/TTF/times.ttf'
    font_size = 12

    with ThreadPoolExecutor(max_workers=multiprocessing.cpu_count()) as executor:
        for csv_file in csv_files:
            executor.submit(convert_to_xlsx, csv_file, font_name, font_file, font_size)

    sys.exit(0)
