#!/usr/bin/env bash

set -eo pipefail

# ./count_output.sh > output_count.txt
# tail -n +2 output_count.txt | grep -Po '(\d+)(?= .*)' | paste -sd'+' | bc
# 26025

# tail -n +2 output_count.txt | grep '^[0-9]' | tr -s ' ' '_' | cut -d'_' -f1,5 | sort -t'_' -n -k2 -k1 | awk -F'_' '{ sums[$2]+=$1 } END { for (caen in sums) { print caen ": " sums[caen] } }'
: '
5829: 763
6201: 7412
6202: 2419
6203: 371
6209: 1277
6311: 2547
6312: 735
7112: 10501
'

# ./count_output.sh true > output_count_filtered.txt
# tail -n +2 output_count_filtered.txt | grep -Po '(\d+)(?= .*)' | paste -sd'+' | bc
# 11364
# tail -n +2 output_count_filtered.txt | grep '^[0-9]' | tr -s ' ' '_' | cut -d'_' -f1,5 | sort -t'_' -n -k2 -k1 | awk -F'_' '{ sums[$2]+=$1 } END { for (caen in sums) { print caen ": " sums[caen] } }'
: '
5829: 531
6201: 3551
6202: 1330
6203: 118
6209: 617
6311: 813
6312: 226
7112: 4178
'
filtered=${1:-false}

input_counties=("Alba" "Arad" "Arges" "Bacau" "Bihor" "Bistrita-nasaud" "Botosani" "Braila" "Brasov" "Bucuresti" "Buzau" "Calarasi" "Caras-severin" "Cluj" "Constanta" "Covasna" "Dambovita" "Dolj" "Galati" "Giurgiu" "Gorj" "Harghita" "Hunedoara" "Ialomita" "Iasi" "Ilfov" "Maramures" "Mehedinti" "Mures" "Neamt" "Olt" "Prahova" "Salaj" "Satu-mare" "Sibiu" "Suceava" "Teleorman" "Timis" "Tulcea" "Valcea" "Vaslui" "Vrancea") 

echo "Number of counties: ${#input_counties[@]}"

# declare -a caen_codes=(5821)
declare -a caen_codes=(6201 6202 7112 5829 6203 6209 6311 6312)

for input_county in "${input_counties[@]}"
do
    echo "$input_county"
    for cod_caen in "${caen_codes[@]}"
    do
        file="Data/$input_county/county_companies_${input_county}_${cod_caen}_out_mfinante.xls"
        
        if [[ "$filtered" = "true" ]];
        then
           file="Data/$input_county/county_companies_${input_county}_${cod_caen}_out_mfinante_sorted_filtered_by_400000_revenue.xls"
           if [[ -s "$file" ]]; then
                echo "$(tail -n +2 "$file" | wc -l) $file"
            fi
        else
            if [[ -s "$file" ]]; then
                wc -l "$file"
            fi
        fi
        
        if [[ -s "$file" ]]; then
            wc -l "$file"
        fi
    done
    echo
done
