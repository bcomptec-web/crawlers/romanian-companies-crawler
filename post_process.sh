#!/usr/bin/env bash

set -ueo pipefail

# ./post_process.sh "Bucuresti" "Ilfov" "Cluj" "Iasi" "Timis" "Brasov" "Sibiu" "Mures" "Bihor" "Arad" "Dolj" "Alba" "Constanta" "Arges" "Bacau" "Bistrita-nasaud" "Botosani" "Braila" "Buzau" "Calarasi" "Caras-severin" "Covasna" "Dambovita" "Galati" "Giurgiu" "Gorj" "Harghita" "Hunedoara" "Ialomita" "Maramures" "Mehedinti" "Neamt" "Olt" "Prahova" "Salaj" "Satu-mare" "Suceava" "Teleorman" "Tulcea" "Valcea" "Vaslui" "Vrancea"

convert=true
deploy=false
remove_empty_files_only=false
default_county="Brasov"
remote_output_dir="/mnt/nas/Data/01_Documents/Romanian_Companies_Financial_Data_Output"
local_output_dir="$(pwd -L)/Romanian_Companies_Financial_Data_Output"

# ls -1 Alba/county_companies_Alba_*.xls | grep -v mfinante | grep -Po '\d+' | sort -u | xargs
declare -a caen_codes=(5829 6201 6202 6203 6209 6311 6312 7112)
# declare -a caen_codes=(5829 6203 6209 6311 6312)

# Waste
# declare -a caen_codes=(3811 3812 3821 3822 3832 4677)

# declare -a caen_codes=(6399)

while [[ $# -ne 0 ]]
do
    county="${1:-$default_county}"

    echo "Post processing $county"

    input_files=()
    
    for caen_code in "${caen_codes[@]}"
    do
        input_files+=("Data/$county/county_companies_${county}_${caen_code}_out_mfinante.xls")
    done
    
    # input_files=("Data/$county/county_companies_${county}_5821_out_mfinante.xls")
    output_files=()

    for input_file in "${input_files[@]}"
    do
        if [[ -s "$input_file" ]]; then
            if [[ ${remove_empty_files_only} = "true" ]]; then
              continue
            fi
            
            output_file="${input_file%.*}"
            number_of_columns=$(awk -F'\t' 'END {print NF}' "$input_file")
            echo "Processing $input_file"
            
            first_file="${output_file}_sorted.xls"
            
            # offset +2
            if ! $(head -n 1 "$input_file" | grep -q '"'); then
                sort -r -t$'\t' -n -k4 -k5 -k6 -k7 "$input_file" > "$first_file"
            else
                sort -r --field-separator='"' -n -k6 -k7 -k8 -k9 "$input_file" > "$first_file"
            fi
            
            output_files+=("$first_file")
            
            if [[ -s "$first_file" ]]; then
                second_file="${output_file}_sorted_filtered_by_400000_revenue.xls"
                gawk -F "\"*\t\"*" '$4 >= 400000 { print; }' "$first_file" > "$second_file"
            fi
            
            if [[ -s "$second_file" ]]; then
              output_files+=("$second_file")
            fi
            
            if [[ "$number_of_columns" -eq 9 ]]; then
                sed -i '1 i\"name"\t"identification number"\t"registration number"\t"turnover"\t"gross profit"\t"net profit"\t"number of employees"\t"locality"\t"url"' "$first_file"
                sed -i '1 i\"name"\t"identification number"\t"registration number"\t"turnover"\t"gross profit"\t"net profit"\t"number of employees"\t"locality"\t"url"' "$second_file"
            else
                sed -i '1 i\"name"\t"identification number"\t"registration number"\t"turnover"\t"gross profit"\t"net profit"\t"number of employees"\t"url"' "$first_file"
                sed -i '1 i\"name"\t"identification number"\t"registration number"\t"turnover"\t"gross profit"\t"net profit"\t"number of employees"\t"url"' "$second_file"
            fi
        else
          echo "Removing empty file: $input_file"
          rm -f "$input_file"
        fi
    done

    if [[ ${remove_empty_files_only} = "true" || ("${#input_files[@]}" -eq 1 && ! -s "$input_file") ]]; then
      shift
      continue
    fi
    
    if [[ ${convert} = "true" ]]; then
        python convert_csv_to_xlsx.py "${output_files[@]}" | tee /tmp/post_process.out
        xlsx_output_files=($(cat /tmp/post_process.out))
        for output_file in ${output_files[@]}; do
            rm "$output_file"
        done
        output_files=( "${xlsx_output_files[@]}" )
    fi

    # echo "${output_files[@]}"
    # exit
    
    ls -l "Data/$county"/*.xls*

    echo "Output files: ${output_files[*]}"
    
    echo "Zipping"
    # ts=$(date "+%d_%B_%Y")
    archive="${county}_companies.zip"

    [[ -s "$archive" ]] && rm "$archive"

    zip "$archive" "${output_files[@]}"
    unzip -l "$archive"

    if [[ -d "/mnt/nas/Data/" && "$deploy" = "true" ]]; then
      [[ -d "$remote_output_dir" ]] || mkdir "$remote_output_dir"
      cp -f "$archive" "$remote_output_dir"
    fi

    [[ -d "$local_output_dir" ]] || mkdir "$local_output_dir"
    mv -f "$archive" "$local_output_dir"

    shift
done

echo "Empty files:"
find Data -type f -empty

echo "Deleting empty files"
find Data -type f -empty -delete

exit 0
