#!/usr/bin/env bash

# Retry mechanism
# go get github.com/mh-cbon/retry
# https://github.com/mh-cbon/retry

# Alternatives:
# https://stackoverflow.com/questions/21982187/bash-loop-until-command-exit-status-equals-0
# https://unix.stackexchange.com/questions/82598/how-do-i-write-a-retry-logic-in-script-to-keep-retrying-to-run-it-upto-5-times

set -o pipefail

# retry -retry-interval 3s -tail wget -q -e robots=off -U 'Mozilla/5.0 (X11; Linux x86_64; rv:63.0) Gecko/20100101 Firefox/63.0' 'https://protonvpn.cwww/' -O content_number_of_companies_test.html
# exit

declare -a input_counties

# Single county
# Major: "Bucuresti", "Ilfov", "Cluj", "Iasi", "Timis", "Brasov", "Sibiu"
default_county="Brasov"
if [[ -n "$1" ]]; then
    desired_county=${1:-$default_county}
    input_counties+=("$desired_county")
    input_county_count=1
else
    # Multiple counties
    # awk -F'\t' '{print $2}' caen_6201_14780_firme_per_judet.xls | sort -u | xargs -I{} printf '"%s" ' {}
    input_counties=("Alba" "Arad" "Arges" "Bacau" "Bihor" "Bistrita-nasaud" "Botosani" "Braila" "Brasov" "Bucuresti" "Buzau" "Calarasi" "Caras-severin" "Cluj" "Constanta" "Covasna" "Dambovita" "Dolj" "Galati" "Giurgiu" "Gorj" "Harghita" "Hunedoara" "Ialomita" "Iasi" "Ilfov" "Maramures" "Mehedinti" "Mures" "Neamt" "Olt" "Prahova" "Salaj" "Satu-mare" "Sibiu" "Suceava" "Teleorman" "Timis" "Tulcea" "Valcea" "Vaslui" "Vrancea")
    # echo "Number of counties: ${#input_counties[@]}"
    input_county_count=1
fi

echo "Input counties: ${input_counties[*]}"

# All
# diff -u <(git show HEAD:Brasov/county_companies_Brasov_6201.xls | sort -t'"' -n -k4) <(cat Brasov/county_companies_Brasov_6201.xls | sort -t'"' -n -k4)

# ./get_input.sh: line 249: unexpected EOF while looking for matching `''
# ./get_input.sh: line 257: syntax error: unexpected end of file

# declare -A caen_codes=( [6399]='Alte activitati de servicii informationale n.c.a.' )

## IT companies
declare -A caen_codes=( 
    [5821]='Activitati de editare a jocurilor de calculator' 
    [5829]='Activitati de editare a altor produse software' 
    [6201]='Activitati de realizare a soft-ului la comanda (software orientat client)' 
    [6202]='Activitati de consultanta in tehnologia informatiei' 
    [6203]='Activitati de management (gestiune si exploatare) a mijloacelor de calcul' 
    [6209]='Alte activitati de servicii privind tehnologia informatiei' 
    [6311]='Prelucrarea datelor, administrarea paginilor web si activitati conexe' 
    [6312]='Activitati ale portalurilor web' 
    [6399]='Alte activitati de servicii informationale n.c.a.)'
)

## Engineering companies
# declare -A caen_codes=( [7112]='Activitati de inginerie si consultanta tehnica legate de acestea' )

# declare -A caen_codes=( [5829]='Activitati de editare a altor produse software' [6203]='Activitati de management (gestiune si exploatare) a mijloacelor de calcul' [6209]='Alte activitati de servicii privind tehnologia informatiei' [6311]='Prelucrarea datelor, administrarea paginilor web si activitati conexe' [6312]='Activitati ale portalurilor web' )

# declare -A caen_codes=( [6201]='Activitati de realizare a soft-ului la comanda (software orientat client)' [6202]='Activitati de consultanta in tehnologia informatiei' [7112]='Activitati de inginerie si consultanta tehnica legate de acestea' [5829]='Activitati de editare a altor produse software' [6203]='Activitati de management (gestiune si exploatare) a mijloacelor de calcul' [6209]='Alte activitati de servicii privind tehnologia informatiei' [6311]='Prelucrarea datelor, administrarea paginilor web si activitati conexe' [6312]='Activitati ale portalurilor web' [4676]='Comert cu ridicata al altor produse intermediare' )

# Waste management companies
# declare -A caen_codes=( [3832]='Recuperarea materialelor reciclabile sortate' [3811]='Colectarea deseurilor nepericuloase' [3821]='Tratarea si eliminarea deseurilor nepericuloase' [3812]='Colectarea deseurilor periculoase' [3822]='Tratarea si eliminarea deseurilor periculoase' [4677]='Comert cu ridicata al deseurilor si resturilor' )

# Game development companies
# declare -A caen_codes=( [5821]='Activitati de editare a jocurilor de calculator' )
# PYTHONUNBUFFERED=1 python3 mfinante.py 5821 "Bucuresti" "Ilfov" "Cluj" "Iasi" "Timis" "Brasov" "Sibiu" "Mures" "Bihor" "Arad" "Dolj" "Alba" "Constanta" "Arges" "Bacau" "Bistrita-nasaud" "Botosani" "Braila" "Buzau" "Calarasi" "Caras-severin" "Covasna" "Dambovita" "Galati" "Giurgiu" "Gorj" "Harghita" "Hunedoara" "Ialomita" "Maramures" "Mehedinti" "Neamt" "Olt" "Prahova" "Salaj" "Satu-mare" "Suceava" "Teleorman" "Tulcea" "Valcea" "Vaslui" "Vrancea" >> mfinante_run.log 2>&1

# declare -A caen_codes=([9511]='Repararea calculatoarelor si a echipamentelor periferice')
# declare -A caen_codes=([9512]='Repararea echipamentelor de comunicatii')

echo '----------------------------------------------------------------------------------------------------------------------------------'

echo "Number of CAEN codes: ${#caen_codes[@]}"

number_of_companies_per_page=50
cold_start=true
expiration_time=2592000 # 30 days

# Termene.ro is broken, shows the company listing in all counties for the county URL: https://termene.ro/cod_caen/6201-Activitati+de+realizare+a+soft%252Dului+la+comanda+%28software+orientat+client%29/Brasov/0
single_page_for_all_counties=1

if [[ ${single_page_for_all_counties} -eq 1 ]]; then
    [[ ! -d "Data/All" ]] && mkdir "Data/All"
fi

for input_county in "${input_counties[@]}"; do
    # if [[ "$input_county" != "Arges" ]]; then
    #  continue
    #fi

    for cod_caen in "${!caen_codes[@]}"; do
        echo '----------------------------------------------------------------------------------------------------------------------------------'
        denumire_caen="${caen_codes[$cod_caen]}"

        if [[ ${input_county_count} -eq 1 ]]; then
            content_number_of_companies_file="Data/content_number_of_companies_${cod_caen}.html"
            if [[ ! -s "$content_number_of_companies_file" || "$(( $(date +"%s") - $(stat -c "%Y" "$content_number_of_companies_file") ))" -gt "$expiration_time" ]]; then
                echo "[$input_county - $cod_caen - $denumire_caen] Getting data for number of companies"
                retry -retry-interval 3s -tail wget -q -e robots=off -U 'Mozilla/5.0 (X11; Linux x86_64; rv:63.0) Gecko/20100101 Firefox/63.0' 'https://termene.ro/cod_caen' -O "$content_number_of_companies_file"
            fi
            
            echo "[$input_county - $cod_caen - $denumire_caen] Parsing number of companies data"
            content=$(xidel -s -e "//*/a[@title=\"$denumire_caen\"]/(div/p/text() || \";\" || @href)" "$content_number_of_companies_file")
            IFS=';' read -ra contentArr <<< "$content"
            number_of_companies=$(echo "${contentArr[0]}" | tr -d ' firme')
            request_uri=${contentArr[1]}
            echo "[$input_county - $cod_caen - $denumire_caen] Number of companies: $number_of_companies"
            # rm "$content_number_of_companies_file"
            
            # https://termene.ro/cod_caen/6202-Activitati+de+consultanta+in+tehnologia+informatiei/0
            # https://termene.ro/cod_caen/6201-Activitati+de+realizare+a+soft%252Dului+la+comanda+%28software+orientat+client%29/0
            content_number_of_companies_per_county_file="Data/content_number_of_companies_per_county_${cod_caen}.html"
            if [[ ! -s "$content_number_of_companies_per_county_file" || "$(( $(date +"%s") - $(stat -c "%Y" "$content_number_of_companies_per_county_file") ))" -gt "$expiration_time" ]]; then
                echo "Getting data for number of companies per county"
                retry -retry-interval 3s -tail wget -q -e robots=off -U 'Mozilla/5.0 (X11; Linux x86_64; rv:63.0) Gecko/20100101 Firefox/63.0' "https://termene.ro$request_uri" -O "$content_number_of_companies_per_county_file"
            fi
            
            echo "[$input_county - $cod_caen - $denumire_caen] Parsing number of companies per county"
            caen_counts_file="Data/caen_${cod_caen}_${number_of_companies}_firme_per_judet.xls"
            xidel -s -e "/html/body/div[3]/div/div/a/(text() || ' ' || 'https://termene.ro' || @href)" "$content_number_of_companies_per_county_file" | sed -e "s/ firme cu cod caen $cod_caen în//g" -e 's/ /\t/g' | sort -k 1 -t, -g -r > "$caen_counts_file"
            
            echo '----------------------------------------------------------------------------------------------------------------------------------'
            gawk -F $'\t' '{ sum+=$1 } END {print "[Check] Number of companies: " sum}' "$caen_counts_file"
        else
            caen_counts_file=$(grep -l Bucuresti "Data/caen_${cod_caen}_"*"_firme_per_judet.xls")
            number_of_companies=$(echo "$caen_counts_file" | cut -d'_' -f 3)
            echo "[$input_county - $cod_caen - $denumire_caen] Number of companies: $number_of_companies"
        fi
        
        while IFS=$'\t' read -ra rows; do
            number_of_companies_in_county="${rows[0]}"
            county="${rows[1]}"
            county_url="${rows[2]}"
            
            # Skip rows until we find the one for current input county
            if [[ "$county" != "$input_county" ]]; then
                continue
            fi
            
            if [[ ! -d "Data/$county" ]]; then
                mkdir "Data/$county"
            fi
            
            if [[ ${single_page_for_all_counties} -eq 1 ]]; then
                number_of_pages=$(( number_of_companies / number_of_companies_per_page ))
                if [[ $(( number_of_companies % number_of_companies_per_page )) -ne 0 ]] ; then
                    number_of_pages=$((number_of_pages+1))
                fi
            else
                number_of_pages=$(( number_of_companies_in_county / number_of_companies_per_page ))
                if [[ $(( number_of_companies_in_county % number_of_companies_per_page )) -ne 0 ]] ; then
                    number_of_pages=$((number_of_pages+1))
                fi
            fi
            
            county_output_file="Data/$county/county_companies_${county}_${cod_caen}.xls"
            
            if [[ ! "$cold_start" == true && -s "$county_output_file" && "$(( $(date +"%s") - $(stat -c "%Y" "$county_output_file") ))" -lt "$expiration_time" && $(wc -l "$county_output_file" | cut -d' ' -f1) -eq $number_of_companies_in_county ]]; then
                echo "Data for county: $county - cod CAEN: $cod_caen already up to date"
                continue
            fi
            
            page=0
            echo "Number of pages: $number_of_pages for $county ($number_of_companies_in_county)"
            
            : > "$county_output_file"
            
            matches_found=0
            
            while [[ ${page} -lt ${number_of_pages} ]]; do
                    county_url="$(dirname "$county_url")/$page"
                    if [[ ${single_page_for_all_counties} -eq 1 ]]; then
                        county_content_file="Data/All/content_companies_${cod_caen}_$page.html"
                    else
                        county_content_file="Data/$county/content_companies_${county}_${cod_caen}_$page.html"
                    fi
                    
                    if [[ ! -s "$county_content_file" || "$(( $(date +"%s") - $(stat -c "%Y" "$county_content_file") ))" -gt "$expiration_time" ]]; then
                        sleep 3
                        echo "Getting companies data for $county ($number_of_companies_in_county) - $page into $county_content_file"
                        retry -retry-interval 3s -tail wget -q -e robots=off -U 'Mozilla/5.0 (X11; Linux x86_64; rv:63.0) Gecko/20100101 Firefox/63.0' "$county_url" -O "$county_content_file"
                    fi
                    
                    echo "Parsing companies per county data for - $county ($number_of_companies_in_county) - $page"
                    # paste <(xidel -s -e "//*/div/h1/a/span/text()" "$county_content_file") <(xidel -s -e "//*/div/div/span[1]/span/text()" "$county_content_file") <(xidel -s -e "//*//div/div/span[2]/text()" "$county_content_file" | sed 's/Numar Inmatriculare RC: //g') <(xidel -s -e "//*/div/div/span[3]/span/text()" "$county_content_file" | awk '{ print toupper($0) }') >> "$county_output_file"
                    
                    tmpfile1="/tmp/1_${county}_${cod_caen}.txt"
                    tmpfile2="/tmp/2_${county}_${cod_caen}.txt"
                    tmpfile3="/tmp/3_${county}_${cod_caen}.txt"
                    tmpfile4="/tmp/4_${county}_${cod_caen}.txt"
                    
                    # Nume firma
                    retry -retry-interval 3s -tail xidel -s -e "//*/div/h1/a/span/text() | //*/div/div[1]/a/span" "$county_content_file" > "$tmpfile1"
                    
                    # Cod unic de identificare
                    retry -retry-interval 3s -tail xidel -s -e "//*/div/div/span[1]/span/text()" "$county_content_file" > "$tmpfile2"
                    
                    if [[ ${single_page_for_all_counties} -eq 1 ]]; then
                      readarray -t cifs < "$tmpfile2"
                    fi
                    
                    # Numar de inregistrare
                    retry -retry-interval 3s -tail xidel -s -e "//*//div/div/span[2]/text()" "$county_content_file" | sed 's/Numar Inmatriculare RC: //g' > "$tmpfile3"
                    
                    # Localitatea
                    retry -retry-interval 3s -tail xidel -s -e "//*/div/div/span[3]/span/text()" "$county_content_file" | awk '{ print toupper($0) }' > "$tmpfile4"

                    if [[ ${single_page_for_all_counties} -eq 1 ]]; then
                        tmpdatafile="/tmp/tmpfile_${county}_${cod_caen}.txt"
                        paste "$tmpfile1" "$tmpfile2" "$tmpfile3" "$tmpfile4" > "$tmpdatafile"
                        
                        # Check each CIF from current page to see if it belongs to the current input county and only adds those to the output file
                        
                        if [[ "${#cifs[@]}" -ge 1 ]]; then
                            # printf '%d\n' "${cifs[@]}"
                            for idx in "${!cifs[@]}"; do
                              cif=$(echo "${cifs[$idx]}" | tr -d '\r\n\t ')
                              #if [[ $cif -eq 32451165 ]]; then
                              #  set -x
                              #fi
                              if grep -q "\b$cif\b" <(gawk -F'^' '{ gsub(/^[ \t]+|[ \t]+$)/, "", $2); print $2; }' "Data/companies_per_county/${input_county}.csv" | grep .); then
                                if grep -q "\b$cif\b" <(gawk -F'\t' '{ gsub(/^[ \t]+|[ \t]+$)/, "", $2); print $2; }' "$tmpdatafile");
                                then
                                  matches_found=$((matches_found+1))
                                  # echo "[$matches_found] Match at index: $idx - $cif"
                                  grep "$cif" "$tmpdatafile" >> "$county_output_file"
                                else
                                  echo "Can't find CIF: $cif in $tmpdatafile"
                                  exit 1
                                fi
                              fi
                              # if [[ $cif -eq 32451165 ]]; then
                              #  exit
                              #fi
                            done
                        fi
                    else
                      paste "$tmpfile1" "$tmpfile2" "$tmpfile3" "$tmpfile4" >> "$county_output_file"
                    fi

                    # PERSOANA FIZICA AUTORIZATA = pfa
                    # INTREPRINDERE INDIVIDUALA = ii
                    # INTREPRINDERE FAMILIALA = if
                    # Find other forms of companies: grep -Pv '(INTREPRINDERE INDIVIDUALA|PERSOANA FIZICA AUTORIZATA|INTREPRINDERE FAMILIALA)' county_companies_Brasov_7112.xls | grep -v SRL
                    # https://stackoverflow.com/questions/1116193/awk-extract-multiple-groups-from-each-line
                    grep -Pv '(INTREPRINDERE INDIVIDUALA|PERSOANA FIZICA AUTORIZATA|PFA|PERSOANA FIZICA|INTREPRINDERE FAMILIALA|ÎNTREPRINDERE FAMILIALĂ|PERSOANĂ FIZICĂ AUTORIZATĂ|ÎNTREPRINDERE INDIVIDUALĂ|PERSOANĂ FIZICĂ)' "$county_output_file" > "${county_output_file}.tmp"
                    mv "${county_output_file}.tmp" "$county_output_file"
                    
                    page=$((page + 1))
            done
            
            actual_number_of_companies_in_county=$(wc -l "$county_output_file" | cut -d' ' -f1)
            echo "$county_output_file - $actual_number_of_companies_in_county"
            
            if [[ "$actual_number_of_companies_in_county" -ne "$number_of_companies_in_county" ]]; then
              echo "[$input_county - $cod_caen - $denumire_caen] Termene.ro has more companies than what was found in ONRC firme neradiate ~ Post: $actual_number_of_companies_in_county, Pre: $number_of_companies_in_county"
            fi
            
            # echo 'O & V DRAGOMIR SRL' | awk '{ pattern="(\\w+) & (\\w+)"; if(match($0, pattern, arr)) { print "here"; print arr[1] " " arr[2]; exit; }  }'
            gawk -i inplace -F $'\t' '{ link=tolower($1 "-" $2); gsub(/[.\x27]/, "", link); pattern="(\\w+) & (\\w+)"; if(match(link, pattern, arr)) { if (length(arr[1]) == 1 && length(arr[2]) == 1) { gsub(/ & /, "", link); } else { gsub(/ & /, "-", link); } } gsub(/& /, "", link); gsub(/ - /, " ", link); gsub(" ", "-", link); gsub(/[&\"()]/, "", link); print "\"" $1 "\"" "\t" "\"" $2 "\"" "\t" "\"" $3 "\"" "\t" "\"" $4 "\"" "\t" "\"" "https://www.totalfirme.ro/" link "\""; }' "$county_output_file"
            sort -t$'\t' -k4,4 -k1,1 -o "$county_output_file" "$county_output_file"
            
            # Move on to the next CAEN code after parsing all the pages for this CAEN code
            break
        done < "$caen_counts_file"
        
        # rm "$content_number_of_companies_per_county_file"
        echo '----------------------------------------------------------------------------------------------------------------------------------'
    done
    
    input_county_count=$((input_county_count+1))
    
done

exit 0
