#!/usr/bin/env bash

set -ueo pipefail

input_counties=("Alba" "Arad" "Arges" "Bacau" "Bihor" "Bistrita-nasaud" "Botosani" "Braila" "Brasov" "Bucuresti" "Buzau" "Calarasi" "Caras-severin" "Cluj" "Constanta" "Covasna" "Dambovita" "Dolj" "Galati" "Giurgiu" "Gorj" "Harghita" "Hunedoara" "Ialomita" "Iasi" "Ilfov" "Maramures" "Mehedinti" "Mures" "Neamt" "Olt" "Prahova" "Salaj" "Satu-mare" "Sibiu" "Suceava" "Teleorman" "Timis" "Tulcea" "Valcea" "Vaslui" "Vrancea") 

declare -a caen_codes=(5821 5829 6201 6202 6203 6209 6311 6312)

count=0
for county in "${input_counties[@]}"
do
  for caen_code in "${caen_codes[@]}"
  do
    output_file="${county}_companies_aggregate.csv"
    > "$output_file"

    filename_xlsx="county_companies_${county}_${caen_code}_out_mfinante_sorted.xlsx"
    filename_csv="${filename_xlsx%.*}.csv"
    
    xlsxfile="Data/$county/${filename_xlsx}"
    if [[ ! -s "$xlsxfile" ]]; then
      echo "Skipping empty file: $xlsxfile"
      continue
    fi
    csvfile="Data/$county/${filename_csv}"
    echo "xlsx=$xlsxfile, csv=$csvfile"
    ./xlsx2csv/xlsx2csv.py -s 1 -d 'tab' -q "all" "$xlsxfile" "$csvfile"
    
    if [[ -s "$input_file" ]]; then
        gawk -F'\t' -v county="$county" 'NR > 1{ print "\"" toupper(county) "\"" "\t" $0 }' "$input_file" >> "$output_file"
        count=$((count+1))
    fi
  done
done

sort -r --field-separator='"' -n -k7 -k8 -k9 -k10 -o "$output_file" "$output_file"

sed -i '1 i\"county\"\t\"name"\t"identification number"\t"registration number"\t"turnover"\t"gross profit"\t"net profit"\t"number of employees"\t"locality"\t"url"' "$output_file"

number_of_companies=$(wc -l "$output_file" | cut -d' ' -f1)
echo "Number of companies: $((number_of_companies-1))"
head -n 100 "$output_file"

exit 0
