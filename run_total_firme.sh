#!/usr/bin/env bash

set -o pipefail

# NOTE: Do the following changes to be able to execute protonvpn cli as non-root user.
# visudo /etc/sudoers
# username ALL=(ALL) NOPASSWD: /bin/protonvpn *

# Retry mechanism: go get https://github.com/mh-cbon/retry

# Usage: ./run_total_firme.sh "Data/companies_per_county/Brasov/xaa.csv" "postgresql://romanian_companies:fln8nv6hfxMoXYRDiNWx@192.168.2.68:5432/romanian_companies"

INPUT_FILE="${1:-$INPUT_FILE}"

DEFAULT_DSN="postgresql://romanian_companies:fln8nv6hfxMoXYRDiNWx@localhost:5432/romanian_companies"
export DSN="${2:-$DEFAULT_DSN}"

if [[ -z "$INPUT_FILE" ]]; then
  echo "Usage: $0 <input_file>"
  exit 1
fi

if ! [[ -x "$(command -v protonvpn)" ]]; then
  echo "Can't find protonvpn Linux CLI client"
  exit 1
fi

echo "Connecting to VPN"
sudo protonvpn disconnect
retry -timeout 30m -retry-interval 3s sudo protonvpn connect --random

sudo protonvpn status

SLEEP_TIME=2

while :    
do
    echo "Running with sleep time: $SLEEP_TIME"
    # "Bucuresti" "Ilfov" "Cluj" "Iasi" "Timis" "Brasov" "Sibiu" "Mures" "Bihor" "Arad" "Dolj" "Alba" "Constanta" "Arges" "Bacau" "Bistrita-nasaud" "Botosani" "Braila" "Buzau" "Calarasi" "Caras-severin" "Covasna" "Dambovita" "Galati" "Giurgiu" "Gorj" "Harghita" "Hunedoara" "Ialomita" "Maramures" "Mehedinti" "Neamt" "Olt" "Prahova" "Salaj" "Satu-mare" "Suceava" "Teleorman" "Tulcea" "Valcea" "Vaslui" "Vrancea"
    python3 -m romanian_companies.crawlers.total_firme --sleep_time $SLEEP_TIME --input_file "$INPUT_FILE" --counties Brasov >> total_firme_run_$(basename "$INPUT_FILE").log 2>&1
    
    exit_status=$?
    
    if [[ "$exit_status" -ne 0 ]]; then
        #SLEEP_TIME=$((SLEEP_TIME+10))
        echo "Retrying by reconnecting to VPN and increasing sleep time to $SLEEP_TIME, exit status was: $exit_status"        
        sudo protonvpn disconnect
        retry -timeout 30m -retry-interval 10s sudo protonvpn connect --random
    else
        echo "Success!"
        break
    fi
done

exit 0
