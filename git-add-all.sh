#!/usr/bin/env bash

set -uo pipefail

add_all=${1:-true}

input_counties=("Alba" "Arad" "Arges" "Bacau" "Bihor" "Bistrita-nasaud" "Botosani" "Braila" "Brasov" "Bucuresti" "Buzau" "Calarasi" "Caras-severin" "Cluj" "Constanta" "Covasna" "Dambovita" "Dolj" "Galati" "Giurgiu" "Gorj" "Harghita" "Hunedoara" "Ialomita" "Iasi" "Ilfov" "Maramures" "Mehedinti" "Mures" "Neamt" "Olt" "Prahova" "Salaj" "Satu-mare" "Sibiu" "Suceava" "Teleorman" "Timis" "Tulcea" "Valcea" "Vaslui" "Vrancea") 

declare -a caen_codes=(6201 6202 7112 5829 6203 6209 6311 6312)

for input_county in "${input_counties[@]}"
do
    echo "$input_county"
    for cod_caen in "${caen_codes[@]}"
    do
        if [[ "$add_all" == true ]]; then
            git add "$input_county"
        else
            file="Data/${input_county}/county_companies_${input_county}_${cod_caen}.xls"
            if [[ -s "$file" ]]; then
                echo "Adding $file"
                if [[ "$input_county" != "Bucuresti" ]]; then
                    grep -i Bucuresti "$file"
                fi
                git add "$file"
            fi
        fi
    done
done

git status

exit 0
