#!/usr/bin/env bash

# grep '&' Brasov/county_companies_Brasov_6201.csv  | awk -F',' '{print $4}' | xargs -I{} -n 1 curl --head -sL -w "%{http_code}\\n" -o /dev/null --connect-timeout 3 --max-time 5 {}

links=( $(awk -F',' '{print $4}' Data/Brasov/county_companies_Brasov_6201.csv Data/Brasov/county_companies_Brasov_6202.csv Data/Brasov/county_companies_Brasov_7112.csv) )

number_of_links="${#links[@]}"
sleep_time=3

echo "Number of links to check: $number_of_links"

eta=$((number_of_links*sleep_time/60))
echo "ETA: $eta minutes"

counter=1
for link in "${links[@]}"
do
    echo "[$counter] Checking $link"
    status_code=$(curl --head -sL -w "%{http_code}\\n" -o /dev/null --connect-timeout 3 --max-time 5 "$link")
    if [[ $status_code -ne 200 ]]; then
        printf "Broken link: %s - %d\n" $link $status_code
    fi
    sleep $sleep_time
    counter=$((counter+1))
done
