#!/usr/bin/env bash

set -uo pipefail

# multitail $(fd _parallel_output.log)

cat Data/counties.txt | xargs -I{} rm {}_parallel_output.log
cat Data/counties.txt | parallel -j $(nproc) './get_input.sh {} >> {}_parallel_output.log'
