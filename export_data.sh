#!/bin/bash

export DSN="postgresql://romanian_companies:fln8nv6hfxMoXYRDiNWx@localhost:5432/romanian_companies"
export CAEN_CODES="4120 4941 5610 6820 5590 4321 4110 5510 3109 7111 7410 4759 3101 4329 3102 4647 5210"
export OUTPUT_DIR=$(pwd)/export

make companies_by_caen_exporter
make FISCAL_YEAR=2019 companies_by_caen_exporter
make FISCAL_YEAR=2018 companies_by_caen_exporter

tree "$OUTPUT_DIR"
ls -lh "$OUTPUT_DIR"
du -sh "$OUTPUT_DIR"