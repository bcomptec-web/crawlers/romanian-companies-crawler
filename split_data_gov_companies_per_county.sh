#!/usr/bin/env bash

set -uo pipefail

input_dir="Data/data.gov.ro"

if [[ ! -d "$input_dir" ]]; then
  echo "Missing input data"
  exit 1
fi

# Data should be downloaded from http://data.gov.ro/organization/onrc (links with: "neradiate cu sediu" in their name) and put into Data/data.gov.ro directory
output_dir="Data/companies_per_county"

[[ ! -d "$output_dir" ]] && mkdir -p "$output_dir"

if [[ ! -s "Data/counties.txt" ]]; then
    grep -hPo 'Bucureşti|(?<=Județ )(.*)' Data/data.gov.ro/*.csv | dos2unix | sort -u > "Data/counties.txt"
fi

readarray -t input_counties < "Data/counties.txt"

echo "Number of counties: ${#input_counties[@]}"

count=1

declare -A countiesNumbers

for input_county in "${input_counties[@]}"
do
    if grep -q "${input_county}" "Data/counties.txt"; then
        case "${input_county}" in
        "Bistriţa-Năsăud")
            output_county="Bistrita-nasaud"
            ;;
        "Caraş-Severin")
            output_county="Caras-severin"
            ;;
        "Satu Mare")
            output_county="Satu-mare"
            ;;
        *)
            output_county=$(echo "${input_county}" | iconv -f UTF-8 -t ascii//TRANSLIT | sed 's/ /-/g' | sed 's/\^//g')
            ;;
        esac      
        
        if [[ "${input_county}" = "Bucureşti" ]]; then
            number_of_companies=$(grep '[\^]' "$input_dir"/3open_data-neradiate_cu_sediu*.csv | grep -chP "Bucureşti Sectorul|Municipiul Bucureşti|Bucureşti S,"  | paste -sd+ | bc)
        else
            number_of_companies=$(grep '[\^]' "$input_dir"/3open_data-neradiate_cu_sediu*.csv | grep -ch "Județ ${input_county}" | paste -sd+ | bc)
        fi
        countiesNumbers["${input_county}"]=$(( number_of_companies - 1 ))
        echo "[$count] Finding companies in ${input_county} from ONRC data. Number of companies - ${number_of_companies}"
        
        # Companies established in all years
        output_file="${output_dir}/${output_county}.csv"
        output_file_free="${output_dir}/${output_county}_free.csv"
        
        filter='(INTREPRINDERE INDIVIDUALA|PERSOANA FIZICA AUTORIZATA|PFA|PERSOANA FIZICA|INTREPRINDERE FAMILIALA|INTREPRINDERE INDIVIDUALĂ|ÎNTREPRINDERE FAMILIALĂ|INTREPRINDERE FAMILIALĂ|PERSOANĂ FIZICĂ AUTORIZATĂ|ÎNTREPRINDERE INDIVIDUALĂ|PERSOANĂ FIZICĂ)'
        if [[ "${input_county}" = "Bucureşti" ]]; then
            # SRL, SRLD, SA
            grep -hP "Bucureşti Sectorul|Municipiul Bucureşti|Bucureşti S," "$input_dir"/3open_data-neradiate_cu_sediu*.csv | grep '[\^]' | awk -F'^' '$2 != 0 && $5 ~ /1048/' | grep -Pv "$filter" > "${output_file}"
            
            # PFA, II, IF
            grep -hP "Bucureşti Sectorul|Municipiul Bucureşti|Bucureşti S," "$input_dir"/3open_data-neradiate_cu_sediu*.csv | grep '[\^]' | awk -F'^' '$2 != 0 && $5 ~ /1048/' | grep -P "$filter" > "${output_file_free}"
        else
            # SRL, SRLD, SA
            grep -h "Județ ${input_county}" "$input_dir"/3open_data-neradiate_cu_sediu*.csv | grep '[\^]' | awk -F'^' '$2 != 0 && $5 ~ /1048/' | grep -Pv "$filter" > "${output_file}"
            
            # PFA, II, IF
            grep -h "Județ ${input_county}" "$input_dir"/3open_data-neradiate_cu_sediu*.csv | grep '[\^]' | awk -F'^' '$2 != 0 && $5 ~ /1048/' | grep -P "$filter" > "${output_file_free}"
        fi
        
        # Add link to totalfirme.ro
        #gawk -i inplace -F^ '{ link=tolower($1 "-" $2); gsub(/[.\x27]/, "", link); pattern="(\\w+) & (\\w+)"; if(match(link, pattern, arr)) { if (length(arr[1]) == 1 && length(arr[2]) == 1) { gsub(/ & /, "", link); } else { gsub(/ & /, "-", link); } } gsub(/& /, "", link); gsub(/ - /, " ", link); gsub(" ", "-", link); gsub(/[&\"()]/, "", link); print "\"" $1 "\"" "^" "\"" $2 "\"" "^" "\"" $3 "\"" "^" "\"" $4 "\"" "^" "\"" "https://www.totalfirme.ro/" link "\""; }'
        #actual_number_of_companies=$(wc -l "${output_file}" | cut -d' ' -f 1)
        #if [[ $actual_number_of_companies -ne $number_of_companies ]]; then
        #    echo "Check failed for ${input_county} ~ Actual: $actual_number_of_companies, Should be: $number_of_companies"
        #fi
        
        dos2unix "${output_file}"
    else
        echo "[$count] Input county: ${input_county} not found!"
        exit 1
    fi       
    
    current_year=$(date +%Y)
    previous_year=$((current_year-1))
    
    # Companies established in current year
    if [[ ! -d "Data/companies_per_county_${current_year}" ]];
    then
        mkdir "Data/companies_per_county_${current_year}"
    fi
    awk -F'^' -v year=$current_year '{ split($3, a, "/"); if(a[3] == year) { print; } }' "${output_file}" > "Data/companies_per_county_${current_year}/${output_county}.csv"
    awk -F'^' -v year=$current_year '{ split($3, a, "/"); if(a[3] == year) { print; } }' "${output_file_free}" > "Data/companies_per_county_${current_year}/${output_county}_free.csv"
    
    # Companies established in previous year
    if [[ ! -d "Data/companies_per_county_${previous_year}" ]];
    then
        mkdir "Data/companies_per_county_${previous_year}"
    fi
    awk -F'^' -v year=$previous_year '{ split($3, a, "/"); if(a[3] == year) { print; } }' "${output_file}" > "Data/companies_per_county_${previous_year}/${output_county}.csv"  
    awk -F'^' -v year=$previous_year '{ split($3, a, "/"); if(a[3] == year) { print; } }' "${output_file_free}" > "Data/companies_per_county_${previous_year}/${output_county}_free.csv"  

    count=$((count+1))
done

IFS=$'\n'; set -f

sorted_values=($(sort -n <<<"${countiesNumbers[*]}"))

sorted_keys=($(
    for key in "${!countiesNumbers[@]}"; do
      printf '%s:%s\n' "$key" "${countiesNumbers[$key]}"
    done | sort -t':' -r -k 2n | sed 's/:.*//'))
unset IFS; set +f

sorted_count="${#sorted_keys[@]}"

for (( cnt=1, index=sorted_count-1; index >= 0; cnt++, index-- ));
do
    county="${sorted_keys[$index]}"
    number_of_companies="${sorted_values[$index]}"
    echo "$cnt: $county - $number_of_companies"
done

exit 0
