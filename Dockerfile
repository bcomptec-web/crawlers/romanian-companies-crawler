FROM python:3.8-alpine3.12

VOLUME /data
VOLUME /config
VOLUME /opt/app/Data

# Configure Go
ENV GOROOT /usr/lib/go
ENV GOPATH /go
ENV PATH /go/bin:$PATH
ENV CGO_ENABLED 0

WORKDIR /opt/app
COPY requirements.txt romanian_companies ./

RUN echo "http://dl-cdn.alpinelinux.org/alpine/v3.12/main" >> /etc/apk/repositories \
    && echo "http://dl-cdn.alpinelinux.org/alpine/v3.12/community" >> /etc/apk/repositories \
    && apk --no-cache add bash dumb-init openvpn curl jq tzdata shadow openrc tinyproxy-openrc chromium chromium-chromedriver git make musl-dev go \
    && mkdir -p ${GOPATH}/src ${GOPATH}/bin \
    && CGO_ENABLED=0 go get github.com/mh-cbon/retry \    
    && LIBRARY_PATH=/lib:/usr/lib /bin/sh -c "pip install --no-cache-dir -r /opt/app/requirements.txt" \
    && rm -rf /tmp/* /var/tmp/* \
    && groupmod -g 1000 users \
    && useradd -K MAIL_DIR=/dev/null -u 911 -U -d /config -s /bin/false abc \
    && usermod -G users abc
    
ADD docker/openvpn/ /etc/openvpn/
ADD docker/romanian-companies-crawler/ /etc/romanian-companies-crawler/
ADD docker/tinyproxy /opt/tinyproxy/
ADD docker/scripts /etc/scripts/
    
ENV OPENVPN_USERNAME=**None** \
    OPENVPN_PASSWORD=**None** \
    OPENVPN_PROVIDER=**None** \
    CREATE_TUN_DEVICE=true \
    PUID= \
    PGID= \
    ROMANIAN_COMPANIES_CRAWLER_HOME=/data/romanian_companies \
    ROMANIAN_COMPANIES_CRAWLER_SCRIPT_NAME=romanian_companies.crawlers.mfinante \
    ROMANIAN_COMPANIES_CRAWLER_SLEEP_TIME=2 \
    ROMANIAN_COMPANIES_CRAWLER_COUNTIES='Brasov' \
    DROP_DEFAULT_ROUTE= \
    WEBPROXY_ENABLED=false \
    WEBPROXY_PORT=8888 \
    WEBPROXY_USERNAME= \
    WEBPROXY_PASSWORD= \
    LOG_TO_STDOUT=false \
    HEALTH_CHECK_HOST=google.com

HEALTHCHECK --interval=1m CMD /etc/scripts/healthcheck.sh
    
# https://github.com/SeleniumHQ/docker-selenium/issues/87
ENV DBUS_SESSION_BUS_ADDRESS=/dev/null

# Add labels to identify this image and version
ARG REVISION

# Set env from build argument or default to empty string
ENV REVISION=${REVISION:-""}

# Run
CMD ["dumb-init", "/etc/openvpn/start.sh"]
