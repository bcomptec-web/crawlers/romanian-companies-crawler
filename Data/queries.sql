DELETE FROM firme_onrc;

SELECT count(*) FROM firme_onrc WHERE judet = 'Brasov'
before: 38995, after: 32217

SELECT count(*) FROM firme_onrc WHERE stare LIKE '%1048%' and judet = 'Brasov'
before: 32217, after: 32217

SELECT count(*) FROM firme_onrc WHERE stare NOT LIKE '%1048%' and judet = 'Brasov'
before: 6778, after: 0

SELECT * FROM firme_onrc WHERE stare NOT LIKE '%1048%' and judet = 'Brasov'
empty set

INSERT INTO firme(cui, nume, cod_inmatriculare, euid, stare, adresa, judet, localitate, total_firme_url, mfinante_url, create_time, update_time)
SELECT fo.* FROM firme_onrc fo LEFT JOIN firme f ON fo.cui = f.cui
WHERE f.cui IS NULL;

SELECT localitate, count(*) as numar_firme
FROM firme
GROUP BY 1
ORDER BY 2 DESC, 1;

SELECT COUNT(*) FROM firme WHERE activitate IS NOT NULL;

ALTER TABLE firme
  ALTER COLUMN mfinante_general_data
  SET DATA TYPE jsonb
  USING mfinante_general_data::jsonb;
  
ALTER TABLE bilant_firme
  ALTER COLUMN mfinante_bilant_data
  SET DATA TYPE jsonb
  USING mfinante_bilant_data::jsonb;
    
SELECT cui, cod_inmatriculare, stare_mfinante, 
  (SELECT string_agg(denumire, ', ')
    FROM stari_firme_onrc 
    WHERE cod IN (SELECT unnest(string_to_array(stare_onrc, ',')::int[]))) as stare_onrc 
FROM firme
WHERE activitate IS NULL AND cod_inmatriculare LIKE '%/2020';

SELECT * FROM firme WHERE activitate IS NULL LIMIT 100 OFFSET 10823;

update import_caen set D_CAEN = REPLACE(D_CAEN, 'Ţ', 'Ț');
update import_caen set D_CAEN = REPLACE(D_CAEN, 'ţ', 'ț');
update import_caen set D_CAEN = REPLACE(D_CAEN, 'Ş', 'Ș');
update import_caen set D_CAEN = REPLACE(D_CAEN, 'ş', 'ș');

--------------------------------------------------------------------------------------------------

# Fix diacritics in activity

SELECT DISTINCT activitate FROM firme ORDER BY activitate;

SELECT COUNT(DISTINCT activitate) FROM firme;
837

SELECT COUNT(DISTINCT unaccent(activitate)) FROM firme;
540

SELECT DISTINCT activitate FROM firme ORDER BY 1;

UPDATE firme SET activitate = unaccent(activitate);

SELECT DISTINCT cod_caen, activitate FROM firme;

# select cod_caen, activitate from firme where activitate ilike '%biotehnologie%';

--------------------------------------------------------------------------------------------------

# Find similar activities
CREATE EXTENSION pg_trgm;

SET pg_trgm.similarity_threshold = 0.8;

CREATE INDEX activitate_trgm_idx ON firme USING GIST (activitate gist_trgm_ops);

SELECT similarity(f1.activitate, f2.activitate) AS sim, f1.activitate, f2.activitate
FROM   firme f1
JOIN   firme f2 ON f1.activitate <> f2.activitate
               AND f1.activitate % f2.activitate
ORDER BY sim DESC;

-- faster
WITH activitati AS (
  SELECT DISTINCT activitate
  FROM firme
  ORDER BY activitate
)
SELECT similarity(f1.activitate, f2.activitate) AS sim, f1.activitate, f2.activitate
FROM   activitati f1
JOIN   activitati f2 ON f1.activitate <> f2.activitate
               AND f1.activitate % f2.activitate
ORDER BY sim DESC;

romanian_companies=# SELECT similarity('Repararea aparatelor electronice, de uz casnic', 'Repararea aparatelor electrocasnice, de uz casnic');
 similarity 
------------
  0.9111111
(1 row)

romanian_companies=# SELECT similarity('Productia de caroserii pentru autovehicule; fabricarea de remorci si semiremorci', 'Productia de caroserii pentru autovehicule, fabricarea de remorci si semiremorci');
 similarity 
------------
          1
(1 row)


--------------------------------------------------------------------------------------------------

UPDATE firme SET update_time = CURRENT_TIMESTAMP, cod_caen = 9499, activitate = 'Activitati ale altor organizatii n.c.a.' WHERE cui = 26667319;
UPDATE coduri_caen SET update_time = CURRENT_TIMESTAMP, denumire = 'Lucrari de constructii a cailor ferate de suprafata si subterane' WHERE cod = 4212;

SELECT cui, cod_inmatriculare, cod_caen FROM firme where activitate is null or activitate = '' order by split_part(cod_inmatriculare, '/', 3) desc;

--------------------------------------------------------------------------------------------------

SELECT activitate, COUNT(*) as count
FROM firme
WHERE judet = 'Brasov' AND activitate IN (
  'Lucrari de constructii a cladirilor rezidentiale si nerezidentiale',
  'Transporturi rutiere de marfuri',
  'Restaurante',
  'Inchirierea si subinchirierea bunurilor imobiliare proprii sau inchiriate',
  'Alte servicii de cazare',
  'Lucrari de instalatii electrice',
  'Dezvoltare (promovare) imobiliara',
  'Hoteluri si alte facilitati de cazare similare',
  'Fabricarea de mobila n.c.a.',
  'Activitati de arhitectura',
  'Activitati de design specializat',
  'Comert cu amanuntul al mobilei, al articolelor de iluminat si al articolelor de uz casnic n.c.a., in magazine specializate',
  'Fabricarea de mobila pentru birouri si magazine',
  'Alte lucrari de instalatii pentru constructii',
  'Fabricarea de mobila pentru bucatarii',
  'Comert cu ridicata al mobilei, covoarelor si a articolelor de iluminat',
  'Depozitari'
)
GROUP BY activitate
ORDER BY count DESC, activitate;

                                                         activitate                                                         | count 
----------------------------------------------------------------------------------------------------------------------------+-------
 Lucrari de constructii a cladirilor rezidentiale si nerezidentiale                                                         |  1974
 Transporturi rutiere de marfuri                                                                                            |  1389
 Restaurante                                                                                                                |   705
 Inchirierea si subinchirierea bunurilor imobiliare proprii sau inchiriate                                                  |   516
 Lucrari de instalatii electrice                                                                                            |   331
 Dezvoltare (promovare) imobiliara                                                                                          |   328
 Alte servicii de cazare                                                                                                    |   287
 Hoteluri si alte facilitati de cazare similare                                                                             |   237
 Activitati de arhitectura                                                                                                  |   229
 Fabricarea de mobila n.c.a.                                                                                                |   197
 Activitati de design specializat                                                                                           |   146
 Comert cu amanuntul al mobilei, al articolelor de iluminat si al articolelor de uz casnic n.c.a., in magazine specializate |    66
 Fabricarea de mobila pentru birouri si magazine                                                                            |    58
 Alte lucrari de instalatii pentru constructii                                                                              |    50
 Fabricarea de mobila pentru bucatarii                                                                                      |    24
 Depozitari                                                                                                                 |    19
 Comert cu ridicata al mobilei, covoarelor si a articolelor de iluminat                                                     |    14
(17 rows)

--------------------------------------------------------------------------------------------------

SELECT activitate, COUNT(*) as count
FROM firme a JOIN bilant_firme b ON a.cui = b.cui AND b.an = 2019
WHERE judet = 'Brasov' AND activitate IN (
  'Lucrari de constructii a cladirilor rezidentiale si nerezidentiale',
  'Transporturi rutiere de marfuri',
  'Restaurante',
  'Inchirierea si subinchirierea bunurilor imobiliare proprii sau inchiriate',
  'Alte servicii de cazare',
  'Lucrari de instalatii electrice',
  'Dezvoltare (promovare) imobiliara',
  'Hoteluri si alte facilitati de cazare similare',
  'Fabricarea de mobila n.c.a.',
  'Activitati de arhitectura',
  'Activitati de design specializat',
  'Comert cu amanuntul al mobilei, al articolelor de iluminat si al articolelor de uz casnic n.c.a., in magazine specializate',
  'Fabricarea de mobila pentru birouri si magazine',
  'Alte lucrari de instalatii pentru constructii',
  'Fabricarea de mobila pentru bucatarii',
  'Comert cu ridicata al mobilei, covoarelor si a articolelor de iluminat',
  'Depozitari'
)
GROUP BY activitate
ORDER BY count DESC, activitate;

                                                         activitate                                                         | count 
----------------------------------------------------------------------------------------------------------------------------+-------
 Lucrari de constructii a cladirilor rezidentiale si nerezidentiale                                                         |  1156
 Transporturi rutiere de marfuri                                                                                            |   960
 Inchirierea si subinchirierea bunurilor imobiliare proprii sau inchiriate                                                  |   425
 Restaurante                                                                                                                |   421
 Dezvoltare (promovare) imobiliara                                                                                          |   238
 Lucrari de instalatii electrice                                                                                            |   237
 Alte servicii de cazare                                                                                                    |   229
 Hoteluri si alte facilitati de cazare similare                                                                             |   186
 Activitati de arhitectura                                                                                                  |   175
 Fabricarea de mobila n.c.a.                                                                                                |   127
 Activitati de design specializat                                                                                           |    88
 Comert cu amanuntul al mobilei, al articolelor de iluminat si al articolelor de uz casnic n.c.a., in magazine specializate |    53
 Fabricarea de mobila pentru birouri si magazine                                                                            |    40
 Alte lucrari de instalatii pentru constructii                                                                              |    36
 Depozitari                                                                                                                 |    14
 Fabricarea de mobila pentru bucatarii                                                                                      |    14
 Comert cu ridicata al mobilei, covoarelor si a articolelor de iluminat                                                     |     7
(17 rows)

--------------------------------------------------------------------------------------------------

-- Find mismatched activity for same CAEN code
select f1.cui, f2.cui, f1.activitate, f2.activitate from firme f1 join firme f2 on f1.cui <> f2.cui and f1.cod_caen = f2.cod_caen and f1.activitate <> f2.activitate;

--------------------------------------------------------------------------------------------------

-- Fix mfinante_balance_data JSON type

select jsonb_typeof(mfinante_bilant_data), count(*) from bilant_firme group by 1 order by 2 desc;
 jsonb_typeof | count 
--------------+-------
 object       | 16140
 string       |  8295
(2 rows)

SELECT jsonb_typeof( ((mfinante_bilant_data::JSONB)->>0)::JSONB ) FROM bilant_firme WHERE an = 2019 AND jsonb_typeof(mfinante_bilant_data) = 'string' LIMIT 1;

SELECT ((mfinante_bilant_data::JSONB)->>0)::JSONB FROM bilant_firme WHERE an = 2019 AND jsonb_typeof(mfinante_bilant_data) = 'string' LIMIT 1;

SELECT ( ((mfinante_bilant_data::JSONB)->>0)::JSONB->'cpp'->'pierdere'->>'brut' )::int FROM bilant_firme WHERE an = 2019 AND jsonb_typeof(mfinante_bilant_data) = 'string' LIMIT 1;

UPDATE bilant_firme SET mfinante_bilant_data = ((mfinante_bilant_data::JSONB)->>0)::JSONB WHERE jsonb_typeof(mfinante_bilant_data) = 'string' AND cui = 41485876;

UPDATE bilant_firme SET mfinante_bilant_data = ((mfinante_bilant_data::JSONB)->>0)::JSONB WHERE jsonb_typeof(mfinante_bilant_data) = 'string';

select (mfinante_bilant_data->'cpp'->'pierdere'->>'brut')::int * (-1) as pierdere_bruta, (mfinante_bilant_data->'cpp'->'pierdere'->>'net')::int * (-1) as pierdere_neta from bilant_firme where cui = 41485876;

--------------------------------------------------------------------------------------------------