iconv -f ISO-8859-2 -t UTF-8 "caen-clase-rev2.csv" > "caen-clase-rev2.csv.tmp"
mv "caen-clase-rev2.csv.tmp" "caen-clase-rev2.csv" 

sed -i 's/Ţ/Ț/g' caen-clase-rev2.csv
sed -i 's/ţ/ț/g' caen-clase-rev2.csv
sed -i 's/Ş/Ș/g' caen-clase-rev2.csv
sed -i 's/ş/ș/g' caen-clase-rev2.csv
