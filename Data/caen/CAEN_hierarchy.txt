# Sectiuni
SECTIUNE	DENUMIRE
J	        INFORMAŢII ŞI COMUNICAŢII

# Diviziuni
DIVIZIUNE	 DENUMIRE	    SECTIUNE
58	Activităţi de editare		J
59	Activităţi de producţie cinematografică, video şi de programe de televiziune; înregistrări audio şi activităţi de editare muzicală		J
60	Activităţi de difuzare şi transmitere de programe		J
61	Telecomunicaţii		J
62	Activităţi de servicii în tehnologia informaţiei		J
63	Activităţi de servicii informatice		J

# Grupe
GRUPA	  DENUMIRE	   DIVIZIUNE
581	Activităţi de editare a cărţilor, ziarelor, revistelor şi alte activităţi de editare		58
582	Activităţi de editare a produselor software		58
591	Activităţi de producţie cinematografică, video şi de programe de televiziune		59
592	Activităţi de realizare a înregistrărilor audio şi activităţi de editare muzicală		59
601	Activităţi de difuzare a programelor de radio		60
602	Activităţi de difuzare a programelor de televiziune		60
611	Activităţi de telecomunicaţii prin reţele cu cablu		61
612	Activităţi de telecomunicaţii prin reţele fără cablu		61
613	Activităţi de telecomunicaţii prin satelit		61
619	Alte activităţi de telecomunicaţii		61
620	Activităţi de servicii în tehnologia informaţiei		62
631	Activităţi ale portalurilor web, prelucrarea datelor, administrarea paginilor web şi activităţi conexe		63
639	Alte activităţi de servicii informaţionale		63

# Clase
CAEN	SECT	DIVIZ	GRUPA	CLASA	CITI	NACE	D_CAEN
5811,00	J		58	581	5811	5811	2211*; 7240*	Activităţi de editare a cărţilor
5812,00	J		58	581	5812	5812	2211*; 7240*	Activităţi de editare de ghiduri, compendii, liste de adrese şi similare
5814,00	J		58	581	5814	5813*	2213; 7240*	Activităţi de editare a revistelor şi periodicelor
5819,00	J		58	581	5819	5819	2215; 2222*; 7240*	Alte activităţi de editare
5821,00	J		58	582	5821	5820*	7221*; 7240*	Activităţi de editare a jocurilor de calculator
5829,00	J		58	582	5829	5820*	7221*; 7240*	Activităţi de editare a altor produse software
5911,00	J		59	591	5911	5911	9211*; 9220*	Activităţi de producţie cinematografică, video şi de programe de televiziune
5912,00	J		59	591	5912	5912	9211*; 9220*	Activităţi de postproducţie cinematografică, video şi de programe de televiziune
5913,00	J		59	591	5913	5913	9212	Activităţi de distribuţie a filmelor cinematografice, video şi a programelor de televiziune
5914,00	J		59	591	5914	5914	9213	Proiecţia de filme cinematografice
5920,00	J		59	592	5920	5920	2214; 7240*; 7487*; 9211*; 9220*	Activităţi de realizare a înregistrărilor audio şi activităţi de editare muzicală
6010,00	J		60	601	6010	6010	6420*; 9220*; 7240*	Activităţi de difuzare a programelor de radio
6020,00	J		60	602	6020	6021	9220*; 6420*; 7240* 7240*	Activităţi de difuzare a programelor de televiziune
6110,00	J		61	611	6110	6110	6420*	Activităţi de telecomunicaţii prin reţele cu cablu
6120,00	J		61	612	6120	6120	6420*	Activităţi de telecomunicaţii prin reţele fără cablu (exclusiv prin satelit)
6130,00	J		61	613	6130	6130	6420*	Activităţi de telecomunicaţii prin satelit
6190,00	J		61	619	6190	6190	6420*	Alte activităţi de telecomunicaţii
6201,00	J		62	620	6201	6201	7221*; 7222*; 7240*	Activităţi de realizare a soft-ului la comandă (software orientat client)
6202,00	J		62	620	6202	6202*	7210; 7222*	Activităţi de consultanţă în tehnologia informaţiei
6203,00	J		62	620	6203	6202*	7230*	Activităţi de management (gestiune şi exploatare) al mijloacelor de calcul
6209,00	J		62	620	6209	6209	3002*; 7222*; 7260	Alte activităţi de servicii privind tehnologia informaţiei
6311,00	J		63	631	6311	6311	7230*; 7240*	Prelucrarea datelor, administrarea paginilor web şi activităţi conexe
6312,00	J		63	631	6312	6312	7240*	Activităţi ale portalurilor web
6391,00	J		63	639	6391	6391	9240*	Activităţi ale agenţiilor de ştiri
