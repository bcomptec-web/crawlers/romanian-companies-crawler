SELECT (SELECT COUNT(*) FROM firme) as number_of_companies,
       (SELECT COUNT(*) FROM firme WHERE activitate IS NULL) as unprocessed_companies, 
       (SELECT COUNT(*) FROM firme WHERE activitate IS NOT NULL) as processed_companies,
       (SELECT COUNT(*) FROM bilant_firme) as number_of_balance_sheets,
       (SELECT COUNT(*) FROM bilant_firme WHERE an = 2019) as number_of_companies_with_balance_in_last_year,
       (SELECT COUNT(*) FROM bilant_firme WHERE an = 2018) as number_of_companies_with_balance_in_previous_to_last_year,
       (SELECT COUNT(*) FROM bilant_firme WHERE an <> 2019) as number_of_companies_without_balance_in_last_year;
       
SELECT judet, localitate, COUNT(*)
FROM firme
GROUP BY judet, localitate
ORDER BY 3 DESC;
